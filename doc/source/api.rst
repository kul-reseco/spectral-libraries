Spectral Library Tool API
=========================

Source code: https://bitbucket.org/kul-reseco/spectral-libraries/src.

For issues, bugs, proposals or remarks, visit the
`issue tracker <https://bitbucket.org/kul-reseco/spectral-libraries/issues?status=new&status=open>`_.

Spectral Library Optimization
-----------------------------

.. toctree::
   :maxdepth: 1

   api/ear_masa_cob
   api/ies
   api/cres
   api/music
   api/amuses


See the User Guide for instructions on using the **Command Line Interface**.

