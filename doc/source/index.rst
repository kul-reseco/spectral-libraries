Spectral Library Tool Documentation
===================================

.. figure:: userguide/images/library_tool.PNG
   :align: center
   :width: 80 %

.. toctree::
   :hidden:

   installation
   userguide
   exercises
   api

.. include:: ../../README.md

.. image:: lumos_big.svg
   :width: 40 %

.. image:: viper_big.svg
   :width: 40 %

.. image:: profile.png
   :width: 10 %

Indexes
-------

* :ref:`genindex`
* :ref:`modindex`

