Exercises
=========

For issues, bugs, proposals or remarks, visit the
`issue tracker <https://bitbucket.org/kul-reseco/spectral-libraries/issues?status=new&status=open>`_.


We have developed two exercises. One with a data set in Brussels (Belgium), and one with a data set in Santa Barbara,
CA (USA).

These exercises consist of two parts: creating and optimizing spectral libraries and MESMA and post-processing:
the former you find here; for the latter visit http://mesma.readthedocs.io.



.. toctree::
   :maxdepth: 1

   exercises/exerciseBrussels
   exercises/exerciseSantaBarbara

