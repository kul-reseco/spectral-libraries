Installation Instructions
=========================

For issues, bugs, proposals or remarks, visit the
`issue tracker <https://bitbucket.org/kul-reseco/spectral-libraries/issues?status=new&status=open>`_.

Installation of QGIS Plugin
---------------------------

The plugin is available in the QGIS Python Plugins Repository::

        Plugins menu > Manage and install plugins... > All > Search for 'Spectral Library Tool'

Alternatively, you can `download <https://bitbucket.org/kul-reseco/spectral-libraries/downloads/>`_ the latest stable
distribution (*spectral-libraries-x-qgis.zip*) and install the plugin manually::

        Plugins menu > Manage and install plugins... > Install from ZIP > Browse to the zip file > Click *Install plugin*.

.. note::

    The plugin is build for QGIS Version 3.6 and up. We recommend to use QGIS version 3.10.
    The plugin has been tested on Windows 10.0, Ubuntu 16.04 and Raspbian GNU/Linux 10 (buster)

Installation of the python package
----------------------------------

Open a shell by running the following batch script (adapt to match with your installation)::

    ::QGIS installation folder
    set OSGEO4W_ROOT=C:\OSGeo4W64

    ::set defaults, clean path, load OSGeo4W modules (incrementally)
    call %OSGEO4W_ROOT%\bin\o4w_env.bat
    call qt5_env.bat
    call py3_env.bat

    ::lines taken from python-qgis.bat
    set QGIS_PREFIX_PATH=%OSGEO4W_ROOT%\apps\qgis
    set PATH=%QGIS_PREFIX_PATH%\bin;%PATH%

    ::make PyQGIS packages available to Python
    set PYTHONPATH=%QGIS_PREFIX_PATH%\python;%PYTHONPATH%

    :: GDAL Configuration (https://trac.osgeo.org/gdal/wiki/ConfigOptions)
    :: Set VSI cache to be used as buffer, see #6448 and
    set GDAL_FILENAME_IS_UTF8=YES
    set VSI_CACHE=TRUE
    set VSI_CACHE_SIZE=1000000
    set QT_PLUGIN_PATH=%QGIS_PREFIX_PATH%\qtplugins;%OSGEO4W_ROOT%\apps\qt5\plugins

    ::enable/disable QGIS debug messages
    set QGIS_DEBUG=1

    ::open the OSGeo4W Shell
    @echo on
    @if [%1]==[] (echo run o-help for a list of available commands & cmd.exe /k) else (cmd /c "%*")

For command-line interface and stand-alone usage, install the python package with pip::

        pip install spectral-libraries

For offline installation, you can `download <https://bitbucket.org/kul-reseco/spectral-libraries/downloads/>`_  the
latest stable distribution (*spectral-libraries-x.tar.gz*) and ::

        C:\WINDOWS\system32>cd C:\Users\UserName\Downloads
        C:\Users\UserName\Downloads>pip install spectral-libraries-x.tar.gz

