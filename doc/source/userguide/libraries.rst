Creating/Editing a Spectral Library
-----------------------------------

So far, QGIS has no functionality for creating, viewing or editing spectral libraries. For that reason, we had to build
this tool ourselves. It has several functions:

 - Create a Spectral Library by manually selecting spectra from an image
 - Create a Spectral Library by using Regions Of Interest (polygons/points)
 - Open and inspect a Spectral Library
 - Edit the metadata of a Spectral Library
 - Remove profiles from the Spectral Library
 - ...

It speaks for itself that a thorough understanding of the research area is required to be able to build a Spectral
Library from it.

.. figure:: images/library_tool.PNG
   :align: center

The table below gives a short overview of all **Spectral Library specific buttons** and their function:

.. |select| image:: images/icons/profile_identify.svg
   :width: 30pt
.. |add| image:: images/icons/plus_green_icon.svg
   :width: 30pt
.. |auto| image:: images/icons/profile_add_auto.svg
   :width: 30pt
.. |import| image:: images/icons/speclib_add.svg
   :width: 30pt
.. |polygon| image:: images/icons/mActionAddOgrLayer.svg
   :width: 30pt
.. |save| image:: images/icons/speclib_save.svg
   :width: 30pt
.. |color| image:: images/icons/speclib_usevectorrenderer.svg
   :width: 30pt

=============  ==================================================================
|select|       **Select a new profile** from a (hyperspectral) image
|add|          **Add** the currently overlaid profiles to the **Spectral Library**
|auto|          **Automatically** add selected profiles to the Spectral Library
|import|       **Import** Spectral Library
|polygon|      Import profiles from **raster + vector source**
|save|         **Save** the current Spectral Library to file
|color|        Use **colors** from map vector symbology
=============  ==================================================================


All other buttons are inherent to QGIS, but here is a short overview:

.. |m_select| image:: images/icons/mActionSelectAll.svg
   :width: 30pt
.. |m_invert| image:: images/icons/mActionInvertSelection.svg
   :width: 30pt
.. |m_deselect| image:: images/icons/mActionDeselectAll.svg
   :width: 30pt
.. |m_pan| image:: images/icons/mActionPanToSelected.svg
   :width: 30pt
.. |m_zoom| image:: images/icons/mActionZoomToSelected.svg
   :width: 30pt
.. |m_reload| image:: images/icons/mActionRefresh.svg
   :width: 30pt
.. |m_edit| image:: images/icons/mActionToggleEditing.svg
   :width: 30pt
.. |m_cut| image:: images/icons/mActionEditCut.svg
   :width: 30pt
.. |m_copy| image:: images/icons/mActionEditCopy.svg
   :width: 30pt
.. |m_paste| image:: images/icons/mActionEditPaste.svg
   :width: 30pt
.. |m_reload_p| image:: images/icons/profile_reload.svg
   :width: 30pt
.. |m_save| image:: images/icons/mActionSaveEdits.svg
   :width: 30pt
.. |m_add| image:: images/icons/mActionNewAttribute.svg
   :width: 30pt
.. |m_rem| image:: images/icons/mActionDeleteAttribute.svg
   :width: 30pt
.. |m_form| image:: images/icons/mActionFormView.svg
   :width: 30pt
.. |m_table| image:: images/icons/mActionOpenTable.svg
   :width: 30pt
.. |m_prop| image:: images/icons/system.svg
   :width: 30pt
.. |m_edit_m| image:: images/icons/mActionMultiEdit.svg
   :width: 30pt
.. |m_add_ft| image:: images/icons/mActionNewTableRow.svg
   :width: 30pt
.. |m_delete| image:: images/icons/mActionDeleteSelectedFeatures.svg
   :width: 30pt
.. |m_expr| image:: images/icons/mIconExpressionSelect.svg
   :width: 30pt
.. |m_top| image:: images/icons/mActionSelectedToTop.svg
   :width: 30pt
.. |m_filter| image:: images/icons/mActionFilter2.svg
   :width: 30pt
.. |m_format| image:: images/icons/mActionConditionalFormatting.svg
   :width: 30pt
.. |m_action| image:: images/icons/mAction.svg
   :width: 30pt

=============  ==================================================================
|m_edit|       Toggle editing mode
|m_edit_m|     Toggle multi-edit mode
|m_save|       Save edits
|m_reload|     Reload the table
|m_add_ft|     Add feature
|m_delete|     Delete selected features
|m_cut|        Cut selected rows to clipboard
|m_copy|       Copy selected rows to clipboard
|m_paste|      Paste features from clipboard
|m_expr|       Select features using an expression
|m_select|     Select all
|m_invert|     Invert selection
|m_deselect|   Deselect all
|m_top|        Move selection to top
|m_filter|     Select/filter features using form
|m_pan|        Pan map to the selected rows
|m_zoom|       Zoom map to the selected rows
|m_add|        New field
|m_rem|        Delete field
|m_format|     Conditional formatting
|m_action|     Actions
|m_form|       Form View
|m_table|      Table View
|m_prop|       Show Spectral Library Properties
=============  ==================================================================

.. note::

   This is a highly interactive tool and has **no command line** equivalent.


Create Spectral Library manually
................................

In order to select spectra from an image, first load an hyperspectral image in QGIS.

 - Toggle the **Select Profile** button |select|.
 - Click on a random pixel in the image to view a profile in the **plot window**. This profile is not automatically
   kept, and you can click as many times as you want in the image, until you find a good profile.
 - Click on the **Add** button |add| to add the last profile to your **Spectral Library**.
 - If you check the **Auto** button |auto| all selected profiles are automatically added to the spectral library.
 - To **remove** a profile, select it in the attribute table and use the **Delete** button |m_delete|.
   In order to do so, you must first toggle editing mode |m_edit| and afterwards save your edits by clicking
   again on |m_edit|.
 - You can now edit the profile metadata, add or remove non-compulsory fields and zoom/pan to your selection on the
   image.
 - Use the settings tool |m_prop| to change the colors of your profiles (according to a metadata element).

.. hint::

   You can view a subset of your data by only setting layer properties for a subset of classes!
   The spectral library behaves like a point shape file and so the styling behaves accordingly.

Create Spectral Library from ROI's
..................................

To start with this step, you need a **ROI (Regions Of Interest)** file. This is a **QGIS Vector Layer**,
with each feature representing an unmixing class (e.g. Pine trees).
These classes, and other metadata, should be included in the attribute table.

 - Make sure both the image and ROI file are open in QGIS.
 - Click on *Import profiles from raster + vector source* |polygon|: a small popup window will ask you to select the
   correct raster and vector files.
   The **All touched** options allows the user to choose between importing only pixels that lie completely within the
   ROI's, or also import pixels that are partly touched by the ROI's boundaries.
 - You can now edit the profile metadata, add or remove non-compulsory fields and zoom/pan to the selection on the image.

.. note::

   A Spectral Library will copy the attribute table of the ROI Vector Layer, it is therefore essential that it
   does not contain the following **protected fields**: 'fid', 'name', 'source', 'values' or 'style'.

   The attribute table of the ROI Vector Layer **must contain an 'ID' field** (spelled with all capitals)
   with unique integer values.

Open existing Spectral Library
..............................

To open an existing Spectral Library, use the **Open Library** button |import|.

Save Spectral Library to file
.............................

To save the current Spectral Library use the **Save Library** button |save|.

.. note::

   All changes made to the Spectral Library exist in memory only, until they are saved to file.


**ACKNOWLEDGMENTS**

This user guide is very loosely based on the VIPER Tools 2.0 user guide (UC Santa Barbara, VIPER Lab):
Roberts, D. A., Halligan, K., Dennison, P., Dudley, K., Somers, B., Crabbe, A., 2018, Viper Tools User Manual,
Version 2, 91 pp.

The Spectral Library tool was created for the most part at HU Berlin, by B. Jakimow.
More information on the source code: https://bitbucket.org/jakimowb/qgispluginsupport.

For issues, bugs, proposals or remarks, visit the
`issue tracker <https://bitbucket.org/kul-reseco/spectral-libraries/issues?status=new&status=open>`_.

