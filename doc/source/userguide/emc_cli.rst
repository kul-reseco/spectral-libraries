Command Line Interface
----------------------

The main command is::

        >emc

Use :code:`-h` or :code:`--help` to list all possible arguments::

        >emc -h

Only the **spectral library** file and the **class name** are **required arguments**. An example::

        >emc "C:\...\Data\spectral_library.sli" LEVEL_2

Use :code:`-r` or :code:`--reflectance-scale` to set the reflectance scale factor (only in case you calculate the square array on the fly).
It can also be automatically derived from the library image as 1, 1 000 or 10 000::

        >emc "C:\...\Data\spectral_library.sli" LEVEL_2 -q "C:\...\Data\square_array.sqr" -r 255

By default, the constraints are set at -0.05 (minimum fraction), 1.05 (maximum fraction) and 0.025 (maximum RMSE).
Use :code:`--min-fraction`, :code:`--max-fraction` and :code:`--max-rmse` arguments to change these values.
To set no constraint for a given variable, use the value -9999.
To use no constraints at all, use the argument :code:`-u` or :code:`--unconstrained`::

        >emc "C:\...\Data\spectral_library.sli" LEVEL_2 --max-fraction 1.10 --max-rmse -9999
        >emc "C:\...\Data\spectral_library.sli" LEVEL_2 -u

By default, the constraints are used in *reset mode* (see explanations before).
To turn this option off, use the argument :code:`-r` or :code:`--reset-off`::

        >emc "C:\...\Data\spectral_library.sli" LEVEL_2 -r

By default, the output file is stored in the same folder as the input file, with the extension '_emc.sli'.
To select another file or another location, use the argument :code:`-o` or :code:`--output`::

        >emc "C:\...\Data\spectral_library.sli" LEVEL_2 -q "C:\...\Data\square_array.sqr" -o "C:\...\Data\emc_library.sli"


**Issue Tracker:**

For issues, bugs, proposals or remarks, visit the
`issue tracker <https://bitbucket.org/kul-reseco/spectral-libraries/issues?status=new&status=open>`_.

