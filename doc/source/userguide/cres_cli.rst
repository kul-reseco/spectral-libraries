Command Line Interface
----------------------

Because of the interactive nature of the CRES algorithm, no CLI is foreseen.


**Issue Tracker:**

For issues, bugs, proposals or remarks, visit the
`issue tracker <https://bitbucket.org/kul-reseco/spectral-libraries/issues?status=new&status=open>`_.

