QGIS GUI
--------

.. figure:: images/cres_gui.PNG
   :align: center

#. Select the *spectral library* containing the spectra you would like to unmix. This library should
   consist of *image spectra* for which field estimates of fractional abundance of the known classes
   (e.g.  green vegetation, soil, and non-photosynthetic vegetation) are available.
   The library can be of any size, but only one spectrum at a time will be assessed with CRES.

#. Once the library has been selected, select the spectrum to be assessed from the drop down list.

#. Select the input spectral library. Note that this library must have metadata attached to it.

#. Select the metadata element that divides the spectra in the library into classes.

#. The reflectance scale factor is automatically detected, but can be changed on the *Advanced* tab.

#. Select the unmixing RMSE constraint.

#. On the advanced tab, it is possible to include non-photometric shade.

#. To calculate SMA fractions and RMSE, click *Calculate SMA fractions*. Do this step only once.

   .. note::

      The table will now be filled with the SMA results (fractions and RMSE) for all models that meet the RMSE
      constraint. Models are identified by the spectra names.

#. Enter estimates of the endmember fraction under the label *Target fractions for INDEX*.

   These estimated fractions may come from field data, expert knowledge or from some other ancillary data.
   These values are a critical component of CRES, as the main goal is to find the spectra that produce the closest fit
   to these endmember fraction estimates.
   Depending on the quality of your initial estimates, you are likely to adjust the fractions.

#. Enter weights for the indices calculations.

   Example for the GV-index in a GV-SOIL-NPV mixture::

     GV index = RMSE weight * RMSE
               + |GV calculated - estimated fraction| * GV weight
               + |SOIL calculated - estimated fraction|
               + |NPV calculated - estimated fraction|
               + |Shade calculated - estimated fraction|

   Some experimentation is needed to assess the best weighting factors.
   Increasing the endmember weight places the greater importance on matching the fraction of the target endmember.
   Increasing the RMSE constraint places greater importance on the fit of the model, or the spectral shape.

   *Note: Because RMSE values (e.g., 0.025) are generally small relative to endmember fraction values (e.g., 0.3) an
   RMSE weight of 5 to 10 is generally helpful in ensuring that RMSE (model fit) is sufficiently weighted.*

   .. note::

      Extra index columns are now added to the table. Play around by changing the index weight values and re-calculating
      the index. **Smaller index numbers indicate better models.**

      The columns can be sorted by clicking on the headers.

#. Based on the results, the user can now select good spectra and export them as a new library.


**Issue Tracker:**

For issues, bugs, proposals or remarks, visit the
`issue tracker <https://bitbucket.org/kul-reseco/spectral-libraries/issues?status=new&status=open>`_.

