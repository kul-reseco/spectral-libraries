Command Line Interface
----------------------

The main command is::

        >square

Use :code:`-h` or :code:`--help` to list all possible arguments::

        >square -h


Only the **spectral library** file is a **required argument**. An example::

        >square "C:\Users\...\Data\spectral_library.sli"

All the other inputs are optional. Use :code:`--reflectance-scale` to set the reflectance scale factor.
It can also be automatically derived from the library image as 1, 1 000 or 10 000::

        >square "C:\Users\...\Data\spectral_library.sli" --reflectance-scale 255

By default, the constraints are set at -0.05 (minimum fraction), 1.05 (maximum fraction) and 0.025 (maximum RMSE).
Use :code:`--min-fraction`, :code:`--max-fraction` and :code:`--max-rmse` arguments to change these values.
To set no constraint for a given variable, use the value -9999.
To use no constraints at all, use the argument :code:`-u` or :code:`--unconstrained`::

        >square "C:\Users\...\Data\spectral_library.sli" --max-fraction 1.10 --max-rmse -9999
        >square "C:\Users\...\Data\spectral_library.sli" -u

By default, the constraints are used in *reset mode* (see explanations before).
To turn this option off, use the argument :code:`-r` or :code:`--reset-off`::

        >square "C:\Users\...\Data\spectral_library.sli" -r

By default, the output file is stored in the same folder as the input file, with the extension '_sq.sqr'. To select
another file or another location, use the argument :code:`-o` or :code:`--output`::

        >square "C:\Users\...\Data\spectral_library.sli" -o "C:\Users\...\Desktop\square_array.sqr"

By default, the following metrics are added to the output: RMSE and Constraints. The Fractions, Shade Fraction and
Spectral Angle are not included by default. Use the following arguments are to reverse these settings:
:code:`--exclude-rmse`, :code:`--exclude-constraints`, :code:`--include-fractions`,
:code:`--include-shade`, :code:`--include-angle`::

        >square "C:\Users\...\Data\spectral_library.sli" --include-fractions --include-angle


**Issue Tracker:**

For issues, bugs, proposals or remarks, visit the
`issue tracker <https://bitbucket.org/kul-reseco/spectral-libraries/issues?status=new&status=open>`_.

