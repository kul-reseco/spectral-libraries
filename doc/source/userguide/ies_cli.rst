Command Line Interface
----------------------

The main command is::

        >ies

Use :code:`-h` or :code:`--help` to list all possible arguments::

        >ies -h

Only the **spectral library** file and the **class name** are **required arguments**. An example::

        >ies "C:\...\Data\spectral_library.sli" LEVEL_2

Use :code:`-r` or :code:`--reflectance-scale` to set the reflectance scale factor (only in case you calculate the square array on the fly).
It can also be automatically derived from the library image as 1, 1 000 or 10 000::

        >ies "C:\...\Data\spectral_library.sli" LEVEL_2 -q "C:\...\Data\square_array.sqr" -r 255

By default, the constraints are set at -0.05 (minimum fraction), 1.05 (maximum fraction) and 0.025 (maximum RMSE).
Use :code:`--min-fraction`, :code:`--max-fraction` and :code:`--max-rmse` arguments to change these values::

        >ies "C:\...\Data\spectral_library.sli" LEVEL_2 --max-fraction 1.0 --max-rmse -0.015

In case you want to use **Forced IES**, use the argument :code:`-f` or :code:`--forced-selection` with an array
with indices of the library spectra that should be forcefully added to the selection. Use the argument :code:`-g` or
:code:`--forced-step` to indicate after which step the endmembers should be added to the selection::

        >ies "C:\...\Data\spectral_library.sli" LEVEL_2 -q "C:\...\Data\square_array.sqr" -f 3 4 25 26 33 -g 1

By default, the output file is stored in the same folder as the input file, with the extension '_ies.sli'. To select
another file or another location, use the argument :code:`-o` or :code:`--output`::

        >ies "C:\...\Data\spectral_library.sli" LEVEL_2 -q "C:\...\Data\square_array.sqr" -o "C:\...\Data\ies_library.sli"


**Issue Tracker:**

For issues, bugs, proposals or remarks, visit the
`issue tracker <https://bitbucket.org/kul-reseco/spectral-libraries/issues?status=new&status=open>`_.

