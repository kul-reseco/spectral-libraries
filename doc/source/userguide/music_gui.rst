MUSIC GUI
---------

.. figure:: images/music_gui.PNG
   :align: center
   :width: 70 %

#. Select the input spectral library.

#. The reflectance scale factor is automatically detected.

    .. note::

       Spectral data can be saved to file as **reflectance values** (i.e. values between 0 and 1)
       or the data can be multiplied by a **scale factor** (usually by 1000 or 10000).
       This is done to be able to store the data as integer values (no comma's), because integers require less memory.
       Although this is more efficient storage-wise,
       it has as a result we have to divide the data values again before processing.

       The software tries to **auto detect** this scale factor, but does not always succeed.
       Double check this value before continuing.

#. Select the input image. Again the reflectance scale factor is automatically detected. Double check it.

#. Advanced settings:

   - The size of the pruned library for MUSIC
   - The minimum number of eigenvectors to be retained from the image to calculate MUSIC distances

#. Select an output filename. A default name is created for you with the structure *libraryName_music.sli*.

**Issue Tracker:**

For issues, bugs, proposals or remarks, visit the
`issue tracker <https://bitbucket.org/kul-reseco/spectral-libraries/issues?status=new&status=open>`_.

