EAR MASA COB Module
===================

.. automodule:: spectral_libraries.core.ear_masa_cob
    :members:
    :undoc-members:
    :show-inheritance:


.. argparse::
   :module: spectral_libraries.interfaces.ear_masa_cob_cli
   :func: create_parser
   :prog: emc

