IES Module
==========

.. automodule:: spectral_libraries.core.ies
    :members:
    :undoc-members:
    :show-inheritance:


.. argparse::
   :module: spectral_libraries.interfaces.ies_cli
   :func: create_parser
   :prog: ies

