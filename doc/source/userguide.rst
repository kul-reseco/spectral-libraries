User Guide
==========

The Spectral Library Tools can be used in several ways:

    1. As a plugin in QGIS
    2. As a command line interface in a terminal
    3. Adapting the code to fulfil very specific needs

For the last option, we refer the user to the
`code repository <https://bitbucket.org/kul-reseco/spectral-libraries/src>`_
and the `API at the end of this document <api.html>`_.

For issues, bugs, proposals or remarks, visit the
`issue tracker <https://bitbucket.org/kul-reseco/spectral-libraries/issues?status=new&status=open>`_.

.. toctree::
   :hidden:

   userguide/libraries
   userguide/optimization


.. figure:: userguide/images/plugin_overview.PNG
   :align: center

