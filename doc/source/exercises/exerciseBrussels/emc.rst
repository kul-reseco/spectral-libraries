Exercise: Pruning the spectral library with EAR-MASA-COB
--------------------------------------------------------

Data used
.........

We will use the Spectral Library (*library.sli*) from the testdata. To analyse this library first, use the
spectral library tool |lib|. In this tool, use the Open Library button |open| and browse to this file.

.. |lib| image:: ../../userguide/images/icons/profile_identify.svg
   :width: 30pt

.. |open| image:: ../../userguide/images/icons/speclib_add.svg
   :width: 30pt

*How many spectra does this library contain?*

*What are the different classes and subclasses in this library and how many are there?*

To help you interpret the different classes, have a look at table 2 in the paper of Jeroen Degerickx (find it in your
exercise folder):

*Degerickx, Roberts, Somers; 2019; Enhancing the performance of Multiple Endmember Spectral Mixture Analysis (MESMA)
for urban land cover mapping using airborne lidar data and band selection; Volume 221; P 260-273*


Exercise part 1 - EMC
.....................

Open the EMC tool |emc| .

.. |emc| image:: ../../userguide/images/icons/emc.PNG
   :width: 20pt

**Spectral Library**:

- Select *library.sli*.
- The reflectance scale factor is automatically detected as 1. This is correct.
- The metadata element we are using for this analysis is **Meta1** (fine classes).

Set **constraints**:

- We leave the constraints settings as they are.

**Output**:

- Change the output name to your liking.
  It is good practice to keep the file in the same folder as the original library.

Click **RUN** to run EMC.

.. figure:: ../images/ex2_emc_gui.PNG
   :align: center
   :width: 70%

Result
......

Open the new library in the Spectral Library Viewer.

The following image shows the tabular output of the algorithm (partly).

.. figure:: ../images/ex2_emc_table.PNG
   :width: 90%


Exercise part 2 - selecting a subset based on the EMC results
.............................................................

Now we will try to prune the library. Our aim is to keep approximately 4 spectra for each of the 28 classes.

.. hint::

   Since these exercises were written, some features were added to the spectral library viewer in order to
   help the user distinguish between different classes. Think about using different colours for different
   classes, not displaying certain classes, and only showing selected features on the screen.
   These features will make this exercise easier to execute than indicated below.


Let’s start with artu (artificial turf):

- The class has originally 22 spectra.
- We see that artu16 was successful in modelling all other spectra in the class (InCOB of 21).
- The EAR values range between 0.009 and 0.031. artu16 has an EAR value of 0.014, not the lowest but quite low. The lowest EAR values are for artu12, artu13, artu15, artu2, artu1 and artu10.
- The MASA values range between 0.090 and 0.210. artu16 has the highest MASA value! The lowest MASA values are for artu1, artu22, artu11, artu18, artu17, artu13 and artu21.
- The only similarities are artu13 and artu1.

How do we explain this?

- Remove all other spectra except for the artu class. Don’t worry, this action does not affect the library saved on file.
- You can see that there are a lot of similar, dark spectra and a few very distinct bright ones.
- artu1 lies in the group of similar spectra, explaining the good MASA value.
- artu12 lies somewhere in the middle of everything, with a form similar to the dark spectra, explaining the good EAR value.
- artu16 is the brightest one with a very different shape – think about why this one would be selected.

We keep those three spectra.

Then we go on with dbt (deciduous broad leaf trees):

- Reload the full library and now remove all spectra except for the dbt class.
- This class contains 29 spectra.
- dbt199 was the only one able to model 27 other spectra (InCOB value of 27). Again this is the most bright spectrum in the class. Does this ring any bell?
- dbt199 also has the best EAR value.
- If you look at the top 5 of MASA values, you see that 4 of them are also in the top 5 of EAR values and vice versa: dbt199, dbt176, dbt189 and dbt198.
- Looking at dbt189 and dbt198 we see that they are (almost) identical.
- We keep the top 3.

Why are EAR and InCOB results so similar, but MASA less?

Go on and select the 3 or 4 best spectra for each class. You can skip the classes Extensive Green Roof, Meadow, Red Gravel, Green Surface, Tartan, Bare Soil and Sand. They are not part of our images.


For issues, bugs, proposals or remarks, visit the
`issue tracker <https://bitbucket.org/kul-reseco/spectral-libraries/issues?status=new&status=open>`_.

