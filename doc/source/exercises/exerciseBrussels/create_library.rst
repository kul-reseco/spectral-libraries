Exercise: Create a Spectral Library
-----------------------------------

Data
....

Use the image *apex_2015_180_smooth*.

Exercise
........

For each land cover class (see previous exercise), try to find several good spectra to add to the Spectral Library. Don't
forget to **add a metadata class for each spectrum**. You won't remember afterwards!

- Turn off the OpenStreetMap layer in order not to get errors.
- Zoom to one of the images.
- Open the tool to build your own Spectral Library |select|.

.. |select| image:: ../../userguide/images/icons/profile_identify.svg
   :width: 30pt

- Click on some random pixels and see the features appear on the plotting window. Add them to your spectral library
  by clicking on the Add button |add|.

.. |add| image:: ../../userguide/images/icons/plus_green_icon.svg
   :width: 30pt

- Toggle the Auto button |auto| to automatically add profile to your spectral library.

.. |auto| image:: ../../userguide/images/icons/profile_add_auto.svg
   :width: 30pt

- To remove a profile, select it in the attribute table and use the Delete button |del|.

.. |del| image:: ../../userguide/images/icons/mActionDeleteSelectedFeatures.svg
   :width: 30pt

- You can now edit the profile metadata, add or remove non-compulsory fields and zoom/pan to selection on the image.

- Save the Spectral Library to the ENVI Spectral Library format (.sli) |save|.

.. |save| image:: ../../userguide/images/icons/speclib_save.svg
   :width: 30pt

Result
......

The resulting Spectral Library can look like this:

.. figure:: ../images/ex2_library_from_pixels.PNG
   :width: 80%

|

For issues, bugs, proposals or remarks, visit the
`issue tracker <https://bitbucket.org/kul-reseco/spectral-libraries/issues?status=new&status=open>`_.

