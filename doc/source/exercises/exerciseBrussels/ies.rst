Exercise: Run IES
-----------------

Data used
.........

We will use the Spectral Library (*library.sli*) from the testdata. To analyse this library first, use the
spectral library tool |lib|. In this tool, use the Open Library button |open| and browse to this file.

.. |lib| image:: ../../userguide/images/icons/profile_identify.svg
   :width: 30pt

.. |open| image:: ../../userguide/images/icons/speclib_add.svg
   :width: 30pt

*How many spectra does this library contain?*

*What are the different classes and subclasses in this library and how many are there?*

To help you interpret the different classes, have a look at table 2 in the paper of Jeroen Degerickx (find it in your
exercise folder):

*Degerickx, Roberts, Somers; 2019; Enhancing the performance of Multiple Endmember Spectral Mixture Analysis (MESMA)
for urban land cover mapping using airborne lidar data and band selection; Volume 221; P 260-273*

Exercise
........

**Spectral Library**:

- Select *library.sli*.
- The reflectance scale factor is automatically detected as 1. This is correct.
- The metadata element we are using for this analysis is **Meta2**.

Set **constraints**:

- We leave the constraints settings as they are.

**Forced IES**:

- We will not use this option in this exercise.

**Output**:

- The default output name is *library_ies.sli*.

Click **RUN** to run IES. The progress should be shown on the log-tab. But since this is a very heavy process, QGIS might
freeze for the duration of the process. IES is quite slow and might take a while.


.. figure:: ../images/ex2_ies_gui.PNG
   :align: center
   :width: 70%


Result
......

A summary of the algorithm's progress is saved as *library_ies_summary.txt* and automatically opens up on the screen.
This summary file lists for each iteration the endmember name, kappa value and confusion matrix::

   IES SUMMARY
   ----------------------------------------

   Based on the spectral library: C:\Users\...\library.sli

   Metadata
       Cass header: Meta2
       Unique classes: EXGR  LVEG  PAVEMENT  ROOF  SHRUB  SOIL  TREE

   Used a forced library? No

   ...

   ITERATIVE PROCESS
   ----------------------------------------

   Loop 0: new endmember: tiler598 (597)
    - Kappa at this point: 0.08008494
    - Confusion Matrix:
                EXGR      LVEG  PAVEMENT      ROOF     SHRUB      SOIL      TREE    Unclas
      EXGR         0         0         0         0         0         0         0         0
      LVEG         0         0         0         0         0         0         0         0
   PAVEMENT        0         0         0         0         0         0         0         0
      ROOF         0         0        36        91         0         2         0         0
     SHRUB         0         0         0         0         0         0         0         0
      SOIL         0         0         0         0         0         0         0         0
      TREE         0         0         0         0         0         0         0         0
    Unclas        14        80       122       124        60        45        59         0

   Loop 1: new endmember: dbt199 (198)
    - Kappa at this point: 0.160418
    - Confusion Matrix:
                EXGR      LVEG  PAVEMENT      ROOF     SHRUB      SOIL      TREE    Unclas
      EXGR         0         0         0         0         0         0         0         0
      LVEG         0         0         0         0         0         0         0         0
   PAVEMENT        0         0         0         0         0         0         0         0
      ROOF         0         0        36        91         0         2         0         0
     SHRUB         0         0         0         0         0         0         0         0
      SOIL         0         0         0         0         0         0         0         0
      TREE         0        11         0         0        32         0        55         0
    Unclas        14        69       122       124        28        45         4         0


From this file, we can see that the first endmember selected was *tiler598*. This is a *roof* spectrum that modeled 91
out of 215 roof spectra but also 36 pavement spectra and 2 soil spectra generating a *kappa* of 0.08.
Thus, using this one spectrum our accuracy would be about 8% and a majority of the library would be unclassified.

Examine the second iteration. You should see *dbt199*. This is a *tree* spectrum that correctly classifies 55 out of 59
tree spectra, but also a majority of the shrub vegetation and some of the low vegetation.
The *kappa* has increased to 0.16.

The final library consists of 92 spectra and the kappa value has reached 0.85.
|

For issues, bugs, proposals or remarks, visit the
`issue tracker <https://bitbucket.org/kul-reseco/spectral-libraries/issues?status=new&status=open>`_.

