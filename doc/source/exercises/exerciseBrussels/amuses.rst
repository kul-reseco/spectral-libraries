Exercise: Run AMUSES
--------------------

Data used
.........

We will use the Spectral Library (*library.sli*) from the testdata. To analyse this library first, use the
spectral library tool |lib|. In this tool, use the Open Library button |open| and browse to this file.

.. |lib| image:: ../../userguide/images/icons/profile_identify.svg
   :width: 30pt

.. |open| image:: ../../userguide/images/icons/speclib_add.svg
   :width: 30pt

*How many spectra does this library contain?*

*What are the different classes and subclasses in this library and how many are there?*

To help you interpret the different classes, have a look at table 2 in the paper of Jeroen Degerickx (find it in your
exercise folder):

*Degerickx, Roberts, Somers; 2019; Enhancing the performance of Multiple Endmember Spectral Mixture Analysis (MESMA)
for urban land cover mapping using airborne lidar data and band selection; Volume 221; P 260-273*

We will also be using the image *apex_2015_180_smooth*. This is a hyperspectral Apex image from 2015 in ENVI format.

Exercise
........

**Spectral Library**:

- Select *library.sli*.
- The reflectance scale factor is automatically detected as 1. This is correct.

**Image**:

- Select *apex_2015_180_smooth*.
- The reflectance scale factor is automatically detected as 1. This is correct.

**Advanced Settings**:

- We will leave all settings as they are.

**Output**:

- The default output name is *library_amuses.sli*.

Click **RUN** to run AMUSES. The progress should be shown on the log-tab.


.. figure:: ../images/ex2_amuses_gui.PNG
   :align: center
   :width: 70%


Result
......

The final library consists of 71 spectra.

.. figure:: ../images/ex2_amuses_library.PNG
   :align: center
   :width: 70%


For issues, bugs, proposals or remarks, visit the
`issue tracker <https://bitbucket.org/kul-reseco/spectral-libraries/issues?status=new&status=open>`_.

