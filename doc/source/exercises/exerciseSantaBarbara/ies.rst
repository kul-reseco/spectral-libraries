Exercise: Run IES in QGIS
-------------------------

Data used
.........

- Spectral Library: roberts2017_urban.sli

Run IES
.......

Select the **input spectral library**.

- Select *roberts2017_urban.sli*.
- The reflectance scale factor is automatically detected as 10000. This is correct.
- The metadata element we are using for this analysis is **Level_7**.

Set **constraints**:

- Minimum fraction to 0.0, maximum to 1.0 and RMSE constraint at 0.025.

**Forced IES**:

- We will not use this option in this exercise.

**Output**:

- The default output name is *roberts2017_urban_ies.sli*.

Click **OK** to run IES. The progress should be shown on the log-tab. But since this is a very heavy process, QGIS might
freeze for the duration of the process. IES is quite slow and might take a while.

.. figure:: ../images/ex1_ies_gui.PNG
   :align: center
   :width: 70%

Result
......

A summary of the algorithm's progress is saved as *roberts2017_urban_ies_summary.txt*. This summary file lists for
each iteration the endmember name, kappa value and confusion matrix::

   IES SUMMARY
   ----------------------------------------

   Based on the spectral library: C:\Users\...\roberts2017_urban_subset.sli

   Metadata
       Cass header: Level_7
       Unique classes: ADFA  ANNUAL_FORB  ARCA-SALE  ARGL  ARTIFICIAL_TURF  ...

   Used a forced library? No

   ...

   ITERATIVE PROCESS
   ----------------------------------------

   Loop 0: new endmember: fscemm.015- (1431)
    - Kappa at this point: 0.061142445
    - Confusion Matrix:
          ...
   Loop 1: new endmember: rpaeom.002- (475)
    - Kappa at this point: 0.098114274
    - Confusion Matrix:
          ...

From this file, we can see that the first endmember selected was *fscemm.015-*. This is a Composite Shingle spectrum
that modeled 804 spectra in the library, of which 300 correct, generating a Kappa of 0.061142445. Thus, using this one
spectrum our accuracy would be about 6%, but a majority of the library would be unclassified.

Examine the second iteration. You should see *rpaeom.002-*. This is a road asphalt spectrum that correctly classifies
166 spectra, resulting in an increase in kappa to 0.098114274.

Examining the Summary.txt file, we can see that kappa continues to increase until Loop 74. At this stage Kappa has
risen to 0.5804497. In the next loop 75, the endmember *CEME04_X1566_Y99* was removed from the list.

The final library consists of 328 spectra (13  spectra were removed along the way).



For issues, bugs, proposals or remarks, visit the
`issue tracker <https://bitbucket.org/kul-reseco/spectral-libraries/issues?status=new&status=open>`_.

