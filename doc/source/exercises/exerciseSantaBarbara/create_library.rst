Exercise: Create a Spectral Library
-----------------------------------

Objective: In this exercise, we will build a Spectral Library, based on an image and it's ROI's (Regions Of Interest).

Data used
.........

- Image: demo1.sub

To display the image as true color image, use RGB wavelengths 0.658, 0.560 and 0.443 |mu| m.

.. |mu|  unicode:: U+003BC .. GREEK SMALL LETTER MU
   :rtrim:

- Regions Of Interest: roi.shp

You might notice a lot of polygons in the ROI layer lie outside the image. This is no problem.
Analyze the metadata: each ROI should have associated metadata like impervious/pervious, vegetation type, etc.
The features represent a mix of natural and anthropogenic materials from needle-leaf shrubs (ADFA) and the invasive
plant species Brassica nigra (BRNI), to asphalt surfaces, red tile roofs, concrete and golf courses.
The metadata headers are generic because there is no hierarchical level common to a plant, a soil or an impervious
surface (what is the species of asphalt, or its life form?).


Automatically build a Spectral Library from ROI's
..................................................

 - Open the *Create Library* tool and click on *Import profiles from raster + vector source*.
 - In the pop-up window, select the shape file as ROI layer and the image as raster layer.
 - Use the *All Touched* option to include all pixels that are partially covered by the ROIs.

Click **OK** to build the library. This might take a few moments.


.. figure:: ../images/ex1_library_from_roi.PNG
   :align: center
   :width: 50%


Result
......

The resulting Spectral Library contains 2557 profiles. Save the library to file.


.. figure:: ../images/ex1_library_result.PNG
   :width: 90%




For issues, bugs, proposals or remarks, visit the
`issue tracker <https://bitbucket.org/kul-reseco/spectral-libraries/issues?status=new&status=open>`_.

