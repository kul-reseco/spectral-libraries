Exercise Santa Barbara
======================

For issues, bugs, proposals or remarks, visit the
`issue tracker <https://bitbucket.org/kul-reseco/spectral-libraries/issues>`_.


Tutorial Data Set
-----------------

You can download the tutorial data set *tutorial_data_set_santa_barbara* from
https://bitbucket.org/kul-reseco/spectral-libraries/downloads/. The zip file contains the following data:

- Image 010614r4_4-5_clip

   A 2001 AVIRIS image (ENVI header format) with 20 m spatial resolution ([Roberts2003]_).

- Image demo1.sub

   A 2011 AVIRIS image (ENVI header format) as 600x600 pixel subset of downtown Santa Barbara ([Roberts2015]_,
   [Roberts2017]_. AVIRIS data were processed to apparent surface reflectance using ATCOR,
   as described in [Roberts2015]_, and scaled to reflectance times 10,000.

- Spectral Library: roberts2017_urban.sli

   This library was used in [Roberts2017]_ and consists of 3725 spectra. The spectra include a mixture of spectra
   extracted from the three AVIRIS flight lines acquired in 2011 merged with the urban spectral library described
   by [Herold2004]_. The AVIRIS spectra, which originally consisted of over 60,000 spectra, were subsampled using
   code developed by Keely Roth as part of her dissertation. Following the protocols described in
   [Roth2012]_, spectra were sampled at the polygon level such that no polygon can contribute more than 10 spectra
   to the library, or more than 50% of the pixels for small polygons. The p9 in the file name, implies this was the
   9th random pull out of 10 pulls. The 9th pull was selected because it produced the highest accuracy (based on IES)
   with the lowest number of spectra, as described in [Roberts2017]_.

- Spectral Library: 010627_westusa_all.sli

   Spectral Library for the CRES tutorial and for Simple SMA.

- Spectral Library: roberts_et_al_2017_final.sli.

   This is the same library that was used to generate the final fraction maps in Roberts et al. (2017).
   The library was created by merging 2,739 spectra extracted from the image with 986 spectra measured in the field.
   IES was run, generating a 497 spectral subset. This library was further pruned to remove poorly performing models
   to generate a 376 endmember subset, then all clearly mixed spectra (assessed visually), were removed to
   generate the *nomix* library, consisting of 259 spectra. Finally, a procedure, designed to remove spectrally
   degenerate spectra was used to generate a 91 endmember subset. The final culling was accomplished in 8 iterations.

- roi.shp

   This contains regions of interest for most dominant cover types in the region. This matches demo1.sub.

**Acknowledgement for the data set:**

*Roberts, D. A., Halligan, K., Dennison, P., Dudley, K., Somers, B., Crabbe, A., 2018, Viper Tools User Manual,
Version 2, 91 pp.*

.. note::

   It is good practice to keep all files in the same folder - especially during the exercises. Files like *square
   arrays* often go looking for library information on which they are built.

Creating Spectral Libraries
---------------------------

.. toctree::
   :maxdepth: 1

   exerciseSantaBarbara/create_library

Spectral Library Optimization
-----------------------------

.. toctree::
   :maxdepth: 1

   exerciseSantaBarbara/emc
   exerciseSantaBarbara/ies
   exerciseSantaBarbara/cres


For issues, bugs, proposals or remarks, visit the
`issue tracker <https://bitbucket.org/kul-reseco/spectral-libraries/issues?status=new&status=open>`_.


**CITATIONS**

.. [Herold2004]
   Herold M, Roberts DA, Gardner ME and Dennison PE. 2004. Spectrometry for urban area remote sensing – Development and
   analysis of a spectral library from 350 to 2400 nm. Remote Sensing of Environment, volume 91, p. 304-319.

.. [Roberts2003]
   Roberts DA, Dennison PE, Gardner M, Hetzel Y, Ustin SL and Lee C. 2003. Evaluation of the Potential of Hyperion for
   Fire Danger Assessment by Comparison to the Airborne Visible/Infrared Imaging Spectrometer.
   IEEE Transactions on Geoscience and Remote Sensing, volume 41, p. 1297-1310.

.. [Roberts2015]
   Roberts DA, Dennison PE, Roth KL, Dudley K and Hulley G. 2015. Relationships Between Dominant Plant Species,
   Fractional Cover and Land Surface Temperature in a Mediterranean Ecosystem. Remote Sensing of Environment, volume 167, p. 152-167.

.. [Roberts2017]
   Roberts DA, Alonzo M, Wetherley E, Dudley K and Dennison P. 2017. Scale in Remote Sensing and GIScience Applications.
   Chapter: Multiscale Analysis of Urban Areas using Mixing Models, p. 247-282.

.. [Roth2012]
   Roth KL, Dennison PE and Roberts DA. 2012. Comparing endmember selection techniques for accurate mapping of plant
   species and land cover using imaging spectrometer data. Remote Sensing of Environment, volume 127, p. 139-152.

