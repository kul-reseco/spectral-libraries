Exercise Brussels
=================

For issues, bugs, proposals or remarks, visit the
`issue tracker <https://bitbucket.org/kul-reseco/spectral-libraries/issues>`_.

Objectives
----------

1.  Learn to work with the Spectral Library Tool in QGIS
2.  Get familiarized with spectral libraries:

   - Build and edit them
   - Prune libraries with optimization techniques like EMC or IES
   - Analyse them with the Library Viewer

3.  Use MESMA as a sub-pixel classification method

    Part 2 on http://mesma.readthedocs.be.


Tutorial Data Set
-----------------

You can download the tutorial data set *tutorial_data_set_brussels* from
https://bitbucket.org/kul-reseco/spectral-libraries/downloads/. The zip file contains the following data:

- Apex images from 2015 with numbers 014, 14 and 180 in ENVI format
- A spectral library in ENVI format
- A validation shape file for each image
- Note: images and library have been smoothed using Savitzky-Golay filter with window size 9

**Acknowledgement for the data set:**

*Degerickx, Roberts, Somers; 2019; Enhancing the performance of Multiple Endmember Spectral Mixture Analysis (MESMA)
for urban land cover mapping using airborne lidar data and band selection; Volume 221; P 260-273*

.. note::

   It is good practice to keep all files in the same folder - especially during the exercises. Files like *square
   arrays* often go looking for library information on which they are built.


Image Inspection
----------------

1. Try to visualize the images in QGIS.

  To recognize the surroundings, overlay them with a map from the OpenStreetMap project (QuickMapServices plugin and
  Google Satellite View).

2. Inspect the technical properties of the image.

  - Why are the images black when first loading them into QGIS? Which bands would you use to visualize them
    for easy interpretation? Look-up the wavelengths of the RGB bands.
  - What is the size of the image and of each pixel?
  - Make a list of the land cover classes you expect to find in each image.

Creating Spectral Libraries
---------------------------

You will find some Spectral Libraries in your data folder. We will not use them for now.
In this exercise, you are supposed to create your own Spectral Library by selecting *pure pixels* from your image.

.. toctree::
   :maxdepth: 1

   exerciseBrussels/create_library

Spectral Library Optimization
-----------------------------

The MESMA algorithm tests every model in your Spectral Library. Let’s say you have 5 classes, with in each class 100
spectra. This leads to 500 2-EM Models, 100 000 3-EM Models, 10 000 000 4-EM Models, … as you can see, this can
quickly get out of hand. Therefore it is good practice to prune your library, i.e. reduce it by removing spectra that
are very similar.

In this exercise, we determine which spectra within a given class (e.g. Soil or Trees) are most representative of
their class, while covering the range of variability within the class.

.. toctree::
   :maxdepth: 1

   exerciseBrussels/amuses
   exerciseBrussels/ies
   exerciseBrussels/emc

Click on these links to find the theory behind
`AMUSES  <../userguide/optimization.html#amuses>`_,
`IES <../userguide/optimization.html#iterative-endmember-selection-ies>`_
and `EMC <../userguide/optimization.html#ear-masa-cob-emc>`_.


For issues, bugs, proposals or remarks, visit the
`issue tracker <https://bitbucket.org/kul-reseco/spectral-libraries/issues?status=new&status=open>`_.

