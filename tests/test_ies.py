# -*- coding: utf-8 -*-
"""
| ----------------------------------------------------------------------------------------------------------------------
| Date                : March 2020
| Copyright           : © 2020 by Ann Crabbé (KU Leuven)
| Email               : acrabbe.foss@gmail.com
|
| This file is part of the Spectral Libraries QGIS plugin and python package.
|
| This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
| License as published by the Free Software Foundation, either version 3 of the License, or any later version.
|
| This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
| warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
|
| You should have received a copy of the GNU General Public License (COPYING.txt). If not see www.gnu.org/licenses.
| ----------------------------------------------------------------------------------------------------------------------
"""
import pathlib
import unittest
import numpy as np

from spectral_libraries.interfaces.imports import import_library_as_array
from spectral_libraries.core.ies import Ies

precision = 5


class TestIES(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        library_path = (pathlib.Path(__file__).resolve().parent / 'data' / 'roberts_et_al_2017_final.sli').as_posix()

        library, cls.names, classes_str = import_library_as_array(library_path, spectra_names=True, metadata='LEVEL_2')
        cls.library = library / 10000
        _, cls.classes_int = np.unique(classes_str, return_inverse=True)

        cls.fix = np.array([4, 8, 24, 43])

    def assertEqualFloatArray(self, array1, array2, decimals):
        x = array1.shape[0]
        for i in range(x):
            self.assertAlmostEqual(array1[i], array2[i], places=decimals)

    def assertEqualStringArray(self, array1, array2):
        x = array1.shape[0]
        for i in range(x):
            self.assertEqual(array1[i], array2[i])

    def test_plain1(self):
        subset, summary = Ies().execute(library=self.library, class_list=self.classes_int, summary=True)
        python_subset = self.names[subset]
        idl_subset = np.array(
            ['basketball_asphalt_field_n1_X1153_Y310', 'airport_asphalt_n2_X1182_Y630', 'bapi_I02_X650_Y127',
             'ndbnyg.002-', 'ndbnye.004-', 'ndbnof.002-', 'ndbnye.011-', 'commercial_roof_n1_X2712_Y146',
             'public_storage_n1_X1446_Y478', 'fscgye.028-', 'parking_structure_p50_concrete_X669_Y202', 'modrmg.004-',
             'spcsye.003-', 'phelps_asphalt&grey_gravel_X1073_Y813', 'golfcourse23_X4051_Y447',
             'golf_course_fairways_1_X2188_Y517', 'green_sports_field_5_X690_Y181', 'senesced_grass_4_X155_Y169',
             'fhzgmg.008-', 'patterson_storage_roof_n1_X1527_Y413', 'REI_white_painted_commercial_roof_n5_X2926_Y762',
             'fish_and_gamedepartment_white_painted_commercial_roof_n2_X2271_Y400',
             'white_painted_commercial_roof_n5_X2692_Y107', 'REI_white_painted_commercial_roof_n5_X2928_Y761',
             'white_painted_commercial_roof_n4_X772_Y589', 'white_painted_commercial_roof_n5_X2699_Y99',
             'sears_commercial_roof_n6_X2233_Y478', 'white_painted_commercial_roof_n2_X851_Y602', 'vtmgyg.006-',
             'PEAM_F01_X1566_Y124', 'RDP_86_X2985_Y343', 'red_tile_n4_X3545_Y723', 'buchanan_red_tile_X786_Y238',
             'red_tile_n4_X3551_Y721', 'red_tile_n2_X2047_Y394', 'RDP_115_X2055_Y368', 'RDP_117_X2491_Y213',
             'RDP_117_X2488_Y217', 'lrxnxx.002-', 'lcxnxx.008-', 'soil_I06_X362_Y211', 'soil_I05_X409_Y385',
             'vbmgue.002-', 'tennis_court_n1_X1320_Y400', 'rotrye.006-', 'fsfnof.002-'], dtype='<U100')
        self.assertEqual(python_subset.shape, idl_subset.shape)
        self.assertEqualStringArray(python_subset, idl_subset)

        python_kappa = np.array([], dtype=float)
        for key in summary:
            python_kappa = np.append(python_kappa, round(summary[key]['kappa'], 6))
        idl_kappa = np.array(
            [0.0821816, 0.150391, 0.189774, 0.226230, 0.263639, 0.299814, 0.334176, 0.364149, 0.388508, 0.412984,
             0.435640, 0.455772, 0.476277, 0.497166, 0.511306, 0.524771, 0.537157, 0.549569, 0.562005, 0.574704,
             0.601460, 0.616369, 0.633065, 0.645812, 0.658191, 0.670624, 0.682697, 0.694809, 0.707010, 0.719301,
             0.731682, 0.744155, 0.756431, 0.768828, 0.781349, 0.793993, 0.806765, 0.819665, 0.832695, 0.845858,
             0.859155, 0.872589, 0.886161, 0.886339, 0.900469, 0.914447])
        self.assertEqual(python_kappa.shape, idl_kappa.shape)
        self.assertEqualFloatArray(python_kappa, idl_kappa, precision)

    def test_plain2(self):
        subset = Ies().execute(library=self.library, class_list=self.classes_int)
        python_subset = self.names[subset]
        idl_subset = np.array(
            ['basketball_asphalt_field_n1_X1153_Y310', 'airport_asphalt_n2_X1182_Y630', 'bapi_I02_X650_Y127',
             'ndbnyg.002-', 'ndbnye.004-', 'ndbnof.002-', 'ndbnye.011-', 'commercial_roof_n1_X2712_Y146',
             'public_storage_n1_X1446_Y478', 'fscgye.028-', 'parking_structure_p50_concrete_X669_Y202', 'modrmg.004-',
             'spcsye.003-', 'phelps_asphalt&grey_gravel_X1073_Y813', 'golfcourse23_X4051_Y447',
             'golf_course_fairways_1_X2188_Y517', 'green_sports_field_5_X690_Y181', 'senesced_grass_4_X155_Y169',
             'fhzgmg.008-', 'patterson_storage_roof_n1_X1527_Y413', 'REI_white_painted_commercial_roof_n5_X2926_Y762',
             'fish_and_gamedepartment_white_painted_commercial_roof_n2_X2271_Y400',
             'white_painted_commercial_roof_n5_X2692_Y107', 'REI_white_painted_commercial_roof_n5_X2928_Y761',
             'white_painted_commercial_roof_n4_X772_Y589', 'white_painted_commercial_roof_n5_X2699_Y99',
             'sears_commercial_roof_n6_X2233_Y478', 'white_painted_commercial_roof_n2_X851_Y602', 'vtmgyg.006-',
             'PEAM_F01_X1566_Y124', 'RDP_86_X2985_Y343', 'red_tile_n4_X3545_Y723', 'buchanan_red_tile_X786_Y238',
             'red_tile_n4_X3551_Y721', 'red_tile_n2_X2047_Y394', 'RDP_115_X2055_Y368', 'RDP_117_X2491_Y213',
             'RDP_117_X2488_Y217', 'lrxnxx.002-', 'lcxnxx.008-', 'soil_I06_X362_Y211', 'soil_I05_X409_Y385',
             'vbmgue.002-', 'tennis_court_n1_X1320_Y400', 'rotrye.006-', 'fsfnof.002-'], dtype='<U100')
        self.assertEqual(python_subset.shape, idl_subset.shape)
        self.assertEqualStringArray(python_subset, idl_subset)

    def test_forced0a(self):
        subset = Ies().execute(library=self.library, class_list=self.classes_int, forced_list=np.array([4]),
                               forced_step=0)
        python_subset = self.names[subset]
        idl_subset = np.array(
            ['basketball_asphalt_field_n1_X1153_Y310', 'airport_asphalt_n2_X1182_Y630', 'BAPI_I05_X237_Y573',
             'bapi_I02_X650_Y127', 'ndbnyg.002-', 'ndbnye.004-', 'ndbnof.002-', 'ndbnye.011-',
             'commercial_roof_n1_X2712_Y146', 'public_storage_n1_X1446_Y478', 'fscgye.028-',
             'parking_structure_p50_concrete_X669_Y202', 'modrmg.004-', 'spcsye.003-',
             'phelps_asphalt&grey_gravel_X1073_Y813', 'golfcourse23_X4051_Y447', 'golf_course_fairways_1_X2188_Y517',
             'green_sports_field_5_X690_Y181', 'senesced_grass_4_X155_Y169', 'fhzgmg.008-',
             'patterson_storage_roof_n1_X1527_Y413', 'REI_white_painted_commercial_roof_n5_X2926_Y762',
             'fish_and_gamedepartment_white_painted_commercial_roof_n2_X2271_Y400',
             'white_painted_commercial_roof_n5_X2692_Y107', 'REI_white_painted_commercial_roof_n5_X2928_Y761',
             'white_painted_commercial_roof_n4_X772_Y589', 'white_painted_commercial_roof_n5_X2699_Y99',
             'sears_commercial_roof_n6_X2233_Y478', 'white_painted_commercial_roof_n2_X851_Y602', 'vtmgyg.006-',
             'PEAM_F01_X1566_Y124', 'RDP_86_X2985_Y343', 'red_tile_n4_X3545_Y723', 'buchanan_red_tile_X786_Y238',
             'red_tile_n4_X3551_Y721', 'red_tile_n2_X2047_Y394', 'RDP_115_X2055_Y368', 'RDP_117_X2491_Y213',
             'RDP_117_X2488_Y217', 'lrxnxx.002-', 'lcxnxx.008-', 'soil_I06_X362_Y211', 'soil_I05_X409_Y385',
             'vbmgue.002-', 'tennis_court_n1_X1320_Y400', 'rotrye.006-', 'fsfnof.002-'], dtype='<U100')
        self.assertEqual(python_subset.shape, idl_subset.shape)
        self.assertEqualStringArray(python_subset, idl_subset)

    def test_forced0b(self):
        subset = Ies().execute(library=self.library, class_list=self.classes_int, forced_list=self.fix, forced_step=0)
        python_subset = self.names[subset]
        idl_subset = np.array(
            ['asphalt_road_surface_1_X2263_Y531', 'airport_asphalt_n2_X1182_Y630', 'BAPI_I05_X237_Y573',
             'bapi_I02_X650_Y127', 'ndbnyg.002-', 'ndbnye.004-', 'ndbnof.002-', 'ndbnye.011-',
             'commercial_roof_n1_X2712_Y146', 'public_storage_n1_X1446_Y478', 'fsctmg.032-', 'fscgye.028-',
             'parking_structure_p10_concrete_X836_Y237', 'parking_structure_p50_concrete_X669_Y202', 'modrmg.004-',
             'spmrye.005-', 'spcsye.003-', 'phelps_asphalt&grey_gravel_X1073_Y813', 'golfcourse23_X4051_Y447',
             'golf_course_fairways_1_X2188_Y517', 'green_sports_field_5_X690_Y181', 'senesced_grass_4_X155_Y169',
             'foznof.001-', 'fhzgmg.008-', 'patterson_storage_roof_n1_X1527_Y413',
             'white_painted_commercial_roof_n1_X629_Y130', 'REI_white_painted_commercial_roof_n5_X2926_Y762',
             'fish_and_gamedepartment_white_painted_commercial_roof_n2_X2271_Y400',
             'white_painted_commercial_roof_n5_X2692_Y107', 'REI_white_painted_commercial_roof_n5_X2928_Y761',
             'white_painted_commercial_roof_n1_X634_Y130', 'white_painted_commercial_roof_n4_X772_Y589',
             'white_painted_commercial_roof_n5_X2699_Y99', 'sears_commercial_roof_n6_X2233_Y478',
             'white_painted_commercial_roof_n2_X851_Y602', 'vtmgyg.006-', 'PEAM_F01_X1566_Y124', 'RDP_86_X2985_Y343',
             'red_tile_n4_X3545_Y723', 'buchanan_red_tile_X786_Y238', 'red_tile_n4_X3551_Y721',
             'red_tile_n2_X2047_Y394', 'RDP_115_X2055_Y368', 'RDP_117_X2491_Y213', 'RDP_117_X2488_Y217', 'lrxnxx.002-',
             'lcxnxx.008-', 'soil_I06_X362_Y211', 'soil_I05_X409_Y385', 'vbmgue.002-', 'tennis_court_n1_X1320_Y400',
             'rotrye.006-', 'fsfnof.002-'], dtype='<U100')
        self.assertEqual(python_subset.shape, idl_subset.shape)
        self.assertEqualStringArray(python_subset, idl_subset)

    def test_forced1(self):
        subset = Ies().execute(library=self.library, class_list=self.classes_int, forced_list=self.fix, forced_step=1)
        python_subset = self.names[subset]
        idl_subset = np.array(
            ['asphalt_road_surface_1_X2263_Y531', 'airport_asphalt_n2_X1182_Y630', 'BAPI_I05_X237_Y573',
             'bapi_I02_X650_Y127', 'ndbnyg.002-', 'ndbnye.004-', 'ndbnof.002-', 'ndbnye.011-',
             'commercial_roof_n1_X2712_Y146', 'public_storage_n1_X1446_Y478', 'fsctmg.032-', 'fscgye.028-',
             'parking_structure_p10_concrete_X836_Y237', 'parking_structure_p50_concrete_X669_Y202', 'modrmg.004-',
             'spmrye.005-', 'spcsye.003-', 'phelps_asphalt&grey_gravel_X1073_Y813', 'golfcourse23_X4051_Y447',
             'golf_course_fairways_1_X2188_Y517', 'green_sports_field_5_X690_Y181', 'senesced_grass_4_X155_Y169',
             'foznof.001-', 'fhzgmg.008-', 'patterson_storage_roof_n1_X1527_Y413',
             'white_painted_commercial_roof_n1_X629_Y130', 'REI_white_painted_commercial_roof_n5_X2926_Y762',
             'fish_and_gamedepartment_white_painted_commercial_roof_n2_X2271_Y400',
             'white_painted_commercial_roof_n5_X2692_Y107', 'REI_white_painted_commercial_roof_n5_X2928_Y761',
             'white_painted_commercial_roof_n1_X634_Y130', 'white_painted_commercial_roof_n4_X772_Y589',
             'white_painted_commercial_roof_n5_X2699_Y99', 'sears_commercial_roof_n6_X2233_Y478',
             'white_painted_commercial_roof_n2_X851_Y602', 'vtmgyg.006-', 'PEAM_F01_X1566_Y124', 'RDP_86_X2985_Y343',
             'red_tile_n4_X3545_Y723', 'buchanan_red_tile_X786_Y238', 'red_tile_n4_X3551_Y721',
             'red_tile_n2_X2047_Y394', 'RDP_115_X2055_Y368', 'RDP_117_X2491_Y213', 'RDP_117_X2488_Y217', 'lrxnxx.002-',
             'lcxnxx.008-', 'soil_I06_X362_Y211', 'soil_I05_X409_Y385', 'vbmgue.002-', 'tennis_court_n1_X1320_Y400',
             'rotrye.006-', 'fsfnof.002-'], dtype='<U100')
        self.assertEqual(python_subset.shape, idl_subset.shape)
        self.assertEqualStringArray(python_subset, idl_subset)

    def test_forced55(self):
        subset = Ies().execute(library=self.library, class_list=self.classes_int, forced_list=self.fix, forced_step=55)
        python_subset = self.names[subset]
        idl_subset = np.array(
            ['basketball_asphalt_field_n1_X1153_Y310', 'airport_asphalt_n2_X1182_Y630', 'bapi_I02_X650_Y127',
             'ndbnyg.002-', 'ndbnye.004-', 'ndbnof.002-', 'ndbnye.011-', 'commercial_roof_n1_X2712_Y146',
             'public_storage_n1_X1446_Y478', 'fscgye.028-', 'parking_structure_p50_concrete_X669_Y202', 'modrmg.004-',
             'spcsye.003-', 'phelps_asphalt&grey_gravel_X1073_Y813', 'golfcourse23_X4051_Y447',
             'golf_course_fairways_1_X2188_Y517', 'green_sports_field_5_X690_Y181', 'senesced_grass_4_X155_Y169',
             'fhzgmg.008-', 'patterson_storage_roof_n1_X1527_Y413', 'REI_white_painted_commercial_roof_n5_X2926_Y762',
             'fish_and_gamedepartment_white_painted_commercial_roof_n2_X2271_Y400',
             'white_painted_commercial_roof_n5_X2692_Y107', 'REI_white_painted_commercial_roof_n5_X2928_Y761',
             'white_painted_commercial_roof_n4_X772_Y589', 'white_painted_commercial_roof_n5_X2699_Y99',
             'sears_commercial_roof_n6_X2233_Y478', 'white_painted_commercial_roof_n2_X851_Y602', 'vtmgyg.006-',
             'PEAM_F01_X1566_Y124', 'RDP_86_X2985_Y343', 'red_tile_n4_X3545_Y723', 'buchanan_red_tile_X786_Y238',
             'red_tile_n4_X3551_Y721', 'red_tile_n2_X2047_Y394', 'RDP_115_X2055_Y368', 'RDP_117_X2491_Y213',
             'RDP_117_X2488_Y217', 'lrxnxx.002-', 'lcxnxx.008-', 'soil_I06_X362_Y211', 'soil_I05_X409_Y385',
             'vbmgue.002-', 'tennis_court_n1_X1320_Y400', 'rotrye.006-', 'fsfnof.002-'], dtype='<U100')
        self.assertEqual(python_subset.shape, idl_subset.shape)
        self.assertEqualStringArray(python_subset, idl_subset)

    def test_multiprocessing(self):
        subset = Ies().execute(library=self.library, class_list=self.classes_int, multiprocessing=True)
        python_subset = self.names[subset]
        idl_subset = np.array(
            ['basketball_asphalt_field_n1_X1153_Y310', 'airport_asphalt_n2_X1182_Y630', 'bapi_I02_X650_Y127',
             'ndbnyg.002-', 'ndbnye.004-', 'ndbnof.002-', 'ndbnye.011-', 'commercial_roof_n1_X2712_Y146',
             'public_storage_n1_X1446_Y478', 'fscgye.028-', 'parking_structure_p50_concrete_X669_Y202', 'modrmg.004-',
             'spcsye.003-', 'phelps_asphalt&grey_gravel_X1073_Y813', 'golfcourse23_X4051_Y447',
             'golf_course_fairways_1_X2188_Y517', 'green_sports_field_5_X690_Y181', 'senesced_grass_4_X155_Y169',
             'fhzgmg.008-', 'patterson_storage_roof_n1_X1527_Y413', 'REI_white_painted_commercial_roof_n5_X2926_Y762',
             'fish_and_gamedepartment_white_painted_commercial_roof_n2_X2271_Y400',
             'white_painted_commercial_roof_n5_X2692_Y107', 'REI_white_painted_commercial_roof_n5_X2928_Y761',
             'white_painted_commercial_roof_n4_X772_Y589', 'white_painted_commercial_roof_n5_X2699_Y99',
             'sears_commercial_roof_n6_X2233_Y478', 'white_painted_commercial_roof_n2_X851_Y602', 'vtmgyg.006-',
             'PEAM_F01_X1566_Y124', 'RDP_86_X2985_Y343', 'red_tile_n4_X3545_Y723', 'buchanan_red_tile_X786_Y238',
             'red_tile_n4_X3551_Y721', 'red_tile_n2_X2047_Y394', 'RDP_115_X2055_Y368', 'RDP_117_X2491_Y213',
             'RDP_117_X2488_Y217', 'lrxnxx.002-', 'lcxnxx.008-', 'soil_I06_X362_Y211', 'soil_I05_X409_Y385',
             'vbmgue.002-', 'tennis_court_n1_X1320_Y400', 'rotrye.006-', 'fsfnof.002-'], dtype='<U100')
        self.assertEqual(python_subset.shape, idl_subset.shape)
        self.assertEqualStringArray(python_subset, idl_subset)
