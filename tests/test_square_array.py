# -*- coding: utf-8 -*-
"""
| ----------------------------------------------------------------------------------------------------------------------
| Date                : March 2020
| Copyright           : © 2020 by Ann Crabbé (KU Leuven)
| Email               : acrabbe.foss@gmail.com
|
| This file is part of the Spectral Libraries QGIS plugin and python package.
|
| This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
| License as published by the Free Software Foundation, either version 3 of the License, or any later version.
|
| This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
| warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
|
| You should have received a copy of the GNU General Public License (COPYING.txt). If not see www.gnu.org/licenses.
| ----------------------------------------------------------------------------------------------------------------------
"""
import pathlib
import numpy as np
import unittest

from spectral_libraries.interfaces.imports import import_library_as_array
from spectral_libraries.core.square_array import SquareArray

max_difference = 0.001
precision = 4


class TestSquareArray(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        file_path = pathlib.Path(__file__).resolve().parent / 'data' / 'spectral_library.sli'
        library, _, _ = import_library_as_array(file_path.as_posix())
        library = library / 10000

        square_array_object = SquareArray()
        python_unconstrained = square_array_object.execute(library, constraints=(), out_constraints=False,
                                                           out_fractions=True, out_shade=True, out_angle=True)

        cls.python_unconstrained_rmse = python_unconstrained['rmse']
        cls.python_unconstrained_angle = python_unconstrained['spectral angle']
        cls.python_unconstrained_frac = python_unconstrained['em fraction']
        cls.python_unconstrained_shade = python_unconstrained['shade fraction']

        python_constrained = square_array_object.execute(library, constraints=(0, 1, 0.025), reset=False,
                                                         out_fractions=True, out_shade=True, out_angle=True)
        cls.python_constrained_rmse = python_constrained['rmse']
        cls.python_constrained_angle = python_constrained['spectral angle']
        cls.python_constrained_frac = python_constrained['em fraction']
        cls.python_constrained_shade = python_constrained['shade fraction']
        cls.python_constrained_const = python_constrained['constraints']

        python_reset = square_array_object.execute(library, constraints=(0, 1, 0.025), reset=True,
                                                   out_fractions=True, out_shade=True, out_angle=True)
        cls.python_reset_rmse = python_reset['rmse']
        cls.python_reset_angle = python_reset['spectral angle']
        cls.python_reset_frac = python_reset['em fraction']
        cls.python_reset_shade = python_reset['shade fraction']
        cls.python_reset_const = python_reset['constraints']

        try:
            square_array_object.execute(library, constraints=(0, 1))
        except Exception as e:
            cls.error = str(e)

    def assertEqualFloatArray(self, array1, array2, decimals):
        if array1.ndim == 1:
            pix = array1.shape[0]
        elif array1.ndim == 2:
            pix = array1.shape[0] * array1.shape[1]
        elif array1.ndim == 3:
            pix = array1.shape[0] * array1.shape[1] * array1.shape[2]
        else:
            assert 0

        array1_line = np.reshape(array1, pix)
        array2_line = np.reshape(array2, pix)
        for i in range(pix):
            self.assertAlmostEqual(array1_line[i], array2_line[i], places=decimals)

    def assertEqualStringArray(self, array1, array2):
        x = array1.shape[0]
        for i in range(x):
            self.assertEqual(array1[i], array2[i])

    def test_fractions_unconstrained(self):
        idl_unconstrained_frac = np.array([[0.,  1.75193202,  2.10109878,  1.01087344,  3.03911138,  0.50104344,  0.5575282,  1.80849814], [0.56022608,  0.,  1.18299985,  0.58180302,  1.78057849,  0.29715636,  0.33000019,  1.0653975 ], [ 0.47357616,  0.83383828,  0.        ,  0.48703575,  1.46585262,  0.24447148,  0.27199373,  0.88109517], [ 0.66167408,  1.19090497,  1.41437531,  0.        ,  2.32270026,  0.46297279,  0.51520598,  1.77327406], [ 0.2800386 ,  0.51308125,  0.59926385,  0.32697684,  0.        ,  0.17420529,  0.19319537,  0.62370116], [ 1.38624072,  2.57099843,  3.00087452,  1.95691228,  5.23062086,  0.        ,  1.11045885,  3.69258285], [ 1.25003207,  2.31378126,  2.70563602,  1.76476872,  4.70088577,  0.89989877,  0.        ,  3.32690287], [ 0.36071521,  0.66452563,  0.779697  ,  0.54034978,  1.3500551 ,  0.26620331,  0.29595959,  0.]])

        self.assertEqual(idl_unconstrained_frac.shape, self.python_unconstrained_frac.shape)
        self.assertEqualFloatArray(idl_unconstrained_frac, self.python_unconstrained_frac, precision)

    def test_fractions_constrained(self):
        idl_constrained_frac = np.array([[0.,  1.75193202,  2.10109878,  1.01087344,  3.03911138,  0.50104344,  0.5575282,  1.80849814], [0.56022608,  0.,  1.18299985,  0.58180302,  1.78057849,  0.29715636,  0.33000019,  1.0653975 ],[ 0.47357616,  0.83383828,  0.        ,  0.48703575,  1.46585262,  0.24447148,  0.27199373,  0.88109517],[ 0.66167408,  1.19090497,  1.41437531,  0.        ,  2.32270026,  0.46297279,  0.51520598,  1.77327406],[ 0.2800386 ,  0.51308125,  0.59926385,  0.32697684,  0.        ,  0.17420529,  0.19319537,  0.62370116],[ 1.38624072,  2.57099843,  3.00087452,  1.95691228,  5.23062086,  0.        ,  1.11045885,  3.69258285],[ 1.25003207,  2.31378126,  2.70563602,  1.76476872,  4.70088577,  0.89989877,  0.        ,  3.32690287],[ 0.36071521,  0.66452563,  0.779697  ,  0.54034978,  1.3500551 ,  0.26620331,  0.29595959,  0.        ]])
        self.assertEqual(idl_constrained_frac.shape, self.python_constrained_frac.shape)
        self.assertEqualFloatArray(idl_constrained_frac, self.python_constrained_frac, precision)

    def test_fractions_reset(self):
        idl_reset_frac = np.array([[0.,  1.,  1.,  1.,  1.,  0.50104344,  0.5575282,  1.], [0.56022608,  0.,  1.,  0.58180302,  1.,  0.29715636,  0.33000019,  1.], [0.47357616,  0.83383828,  0.,  0.48703575,  1.        ,  0.24447148,  0.27199373,  0.88109517],[ 0.66167408,  1.        ,  1.        ,  0.        ,  1.        ,  0.46297279,  0.51520598,  1.        ],[ 0.2800386 ,  0.51308125,  0.59926385,  0.32697684,  0.        ,  0.17420529,  0.19319537,  0.62370116],[ 1.        ,  1.        ,  1.        ,  1.        ,  1.        ,  0.        ,  1.        ,  1.        ],[ 1.        ,  1.        ,  1.        ,  1.        ,  1.        ,  0.89989877,  0.        ,  1.        ],[ 0.36071521,  0.66452563,  0.779697  ,  0.54034978,  1.        ,  0.26620331,  0.29595959,  0.        ]])
        self.assertEqual(idl_reset_frac.shape, self.python_reset_frac.shape)
        self.assertEqualFloatArray(idl_reset_frac, self.python_reset_frac, precision)

    def test_shade_unconstrained(self):
        idl_unconstrained_shade = np.array([[0., -0.75193202, -1.10109878, -0.01087344, -2.03911138,  0.49895656,  0.4424718 , -0.80849814], [0.43977392,  0., -0.18299985,  0.41819698, -0.78057849,  0.70284367,  0.66999984, -0.0653975 ], [0.52642381,  0.16616172,  0.,  0.51296425, -0.46585262,  0.75552851,  0.72800624,  0.11890483], [0.33832592, -0.19090497, -0.41437531,  0., -1.32270026,  0.53702724,  0.48479402, -0.77327406],[0.7199614 ,  0.48691875,  0.40073615,  0.67302316,  0.,  0.8257947 ,  0.80680466,  0.37629884],[-0.38624072, -1.57099843, -2.00087452, -0.95691228, -4.23062086,  0.        , -0.11045885, -2.69258285],[-0.25003207, -1.31378126, -1.70563602, -0.76476872, -3.70088577,  0.10010123,  0.        , -2.32690287],[0.63928479,  0.33547437,  0.220303  ,  0.45965022, -0.3500551 ,  0.73379672,  0.70404041,  0.        ]])
        self.assertEqual(idl_unconstrained_shade.shape, self.python_unconstrained_shade.shape)
        self.assertEqualFloatArray(idl_unconstrained_shade, self.python_unconstrained_shade, precision)

    def test_shade_constrained(self):
        idl_constrained_shade = np.array([[0., -0.75193202, -1.10109878, -0.01087344, -2.03911138,  0.49895656,  0.4424718, -0.80849814], [0.43977392,  0., -0.18299985,  0.41819698, -0.78057849,  0.70284367,  0.66999984, -0.0653975], [0.52642381,  0.16616172,  0.,  0.51296425, -0.46585262,  0.75552851,  0.72800624,  0.11890483], [0.33832592, -0.19090497, -0.41437531,  0., -1.32270026,  0.53702724,  0.48479402, -0.77327406], [0.7199614,  0.48691875,  0.40073615,  0.67302316,  0.,  0.8257947,  0.80680466,  0.37629884], [-0.38624072, -1.57099843, -2.00087452, -0.95691228, -4.23062086,  0., -0.11045885, -2.69258285], [-0.25003207, -1.31378126, -1.70563602, -0.76476872, -3.70088577,  0.10010123,  0., -2.32690287], [0.63928479,  0.33547437,  0.220303,  0.45965022, -0.3500551,  0.73379672,  0.70404041,  0.]])
        self.assertEqual(idl_constrained_shade.shape, self.python_constrained_shade.shape)
        self.assertEqualFloatArray(idl_constrained_shade, self.python_constrained_shade, precision)

    def test_shade_reset(self):
        idl_reset_shade = np.array([[0.,  0.,  0.,  0.,  0.,  0.49895656,  0.4424718,  0.], [0.43977392,  0.,  0.,  0.41819698,  0.,  0.70284367,  0.66999984,  0.], [0.52642381,  0.16616172,  0.,  0.51296425,  0.,  0.75552851,  0.72800624,  0.11890483], [0.33832592,  0.,  0.,  0.,  0.,  0.53702724,  0.48479402,  0.], [0.7199614,  0.48691875,  0.40073615,  0.67302316,  0.,  0.8257947,  0.80680466,  0.37629884], [0.,  0.,  0.,  0.,  0.,  0.,  0.,  0.], [0.,  0.,  0.,  0.,  0.,  0.10010123,  0.,  0.], [0.63928479,  0.33547437,  0.220303,  0.45965022,  0.,  0.73379672,  0.70404041,  0.]])
        self.assertEqual(idl_reset_shade.shape, self.python_reset_shade.shape)
        self.assertEqualFloatArray(idl_reset_shade, self.python_reset_shade, precision)

    def test_rmse_unconstrained(self):
        idl_unconstrained_rmse = np.array([[0.,  0.0048257,  0.00297738,  0.01426155,  0.02549161,  0.00666218,  0.00737201,  0.02647196], [0.00272887,  0.,  0.00491983,  0.01373492,  0.01941824,  0.00585632,  0.00651155,  0.02426164], [0.00141353,  0.00413046,  0.,  0.01382453,  0.02303106,  0.0062216,  0.00688151,  0.02511872], [0.01153826,  0.01965063,  0.02355875,  0.,  0.03239593,  0.00369597,  0.00403469,  0.00918052], [0.00773808,  0.01042371,  0.01472576,  0.01215493,  0.,  0.00359219,  0.00405751,  0.01784446], [0.01108149,  0.01722594,  0.02179776,  0.00759866,  0.01968365,  0.,  0.00035414,  0.00585754], [0.01103857,  0.01724202,  0.02170397,  0.00746731,  0.02001479,  0.0003188,  0.,  0.0055664], [0.0118225,  0.01916108,  0.0236292,  0.00506777,  0.02625374,  0.00157274,  0.00166024,  0.]])
        self.assertEqual(idl_unconstrained_rmse.shape, self.python_unconstrained_rmse.shape)
        self.assertEqualFloatArray(idl_unconstrained_rmse, self.python_unconstrained_rmse, precision)

    def test_rmse_constrained(self):
        idl_constrained_rmse = np.array([[0.,  0.0048257,  0.00297738,  0.01426155,  0.02549161,  0.00666218,  0.00737201,  0.02647196], [0.00272887,  0.,  0.00491983,  0.01373492,  0.01941824,  0.00585632,  0.00651155,  0.02426164], [0.00141353,  0.00413046,  0.,  0.01382453,  0.02303106,  0.0062216,  0.00688151,  0.02511872], [0.01153826,  0.01965063,  0.02355875,  0.,  0.03239593,  0.00369597,  0.00403469,  0.00918052], [0.00773808,  0.01042371,  0.01472576,  0.01215493,  0.,  0.00359219,  0.00405751,  0.01784446], [0.01108149,  0.01722594,  0.02179776,  0.00759866,  0.01968365,  0.,  0.00035414,  0.00585754], [0.01103857,  0.01724202,  0.02170397,  0.00746731,  0.02001479,  0.0003188,  0.,  0.0055664], [0.0118225,  0.01916108,  0.0236292,  0.00506777,  0.02625374,  0.00157274,  0.00166024,  0.]])
        self.assertEqual(idl_constrained_rmse.shape, self.python_constrained_rmse.shape)
        self.assertEqualFloatArray(idl_constrained_rmse, self.python_constrained_rmse, precision)

    def test_rmse_reset(self):
        idl_reset_rmse = np.array([[0.,  0.01583056,  0.02227817,  0.01426322,  0.04818232,  0.00666218,  0.00737201,  0.03104146], [0.00272887,  0.,  0.00814309,  0.01373492,  0.03381026,  0.00585632,  0.00651155,  0.0243722], [0.00141353,  0.00413046,  0.,  0.01382453,  0.03029089,  0.0062216,  0.00688151,  0.02511872], [0.01153826,  0.02021219,  0.02569985,  0.,  0.04608811,  0.00369597,  0.00403469,  0.02125002], [0.00773808,  0.01042371,  0.01472576,  0.01215493,  0.,  0.00359219,  0.00405751,  0.01784446], [0.0120199,  0.02560042,  0.03251031,  0.01381318,  0.05466585,  0.,  0.00137785,  0.03298273], [0.01153518,  0.0246332,  0.03150771,  0.01267435,  0.05344754,  0.0003188,  0.,  0.03165283], [0.0118225,  0.01916108,  0.0236292,  0.00506777,  0.03059844,  0.00157274,  0.00166024,  0.]])
        self.assertEqual(idl_reset_rmse.shape, self.python_reset_rmse.shape)
        self.assertEqualFloatArray(idl_reset_rmse, self.python_reset_rmse, precision)

    def test_angle_unconstrained(self):
        idl_unconstrained_angle = np.array([[0., 0.13652141, 0.07055733, 0.61314315, 0.39620084, 0.58555251, 0.58298725, 0.63058382], [0.13652141, 0., 0.1167507, 0.58739132, 0.29837909, 0.50728887, 0.50780809, 0.57089478], [0.07055733, 0.1167507, 0., 0.59174138, 0.35614809, 0.54230458, 0.53971428, 0.59375286], [0.61314315, 0.58739132, 0.59174138, 0., 0.51259488, 0.31161687, 0.30605486, 0.20593329], [0.39620084, 0.29837909, 0.35614809, 0.51259488, 0., 0.30258512, 0.3078427, 0.40874094], [0.58555251, 0.50728887, 0.54230458, 0.31161687, 0.30258512, 0., 0.02644245, 0.13084012],  [0.58298725, 0.50780809, 0.53971428, 0.30605486, 0.3078427, 0.02644245, 0., 0.12430177], [0.63058382, 0.57089478, 0.59375286, 0.20593329, 0.40874094, 0.13084012, 0.12430177, 0.]])
        self.assertEqual(idl_unconstrained_angle.shape, self.python_unconstrained_angle.shape)
        self.assertEqualFloatArray(idl_unconstrained_angle, self.python_unconstrained_angle, precision)

    def test_angle_constrained(self):
        idl_constrained_angle = np.array([[0.,  0.13652141,  0.07055733,  0.61314315,  0.39620084,  0.58555251,  0.58298725,  0.63058382], [0.13652141,  0.,  0.1167507,  0.58739132,  0.29837909,  0.50728887,  0.50780809,  0.57089478], [0.07055733,  0.1167507,  0.,  0.59174138,  0.35614809,  0.54230458,  0.53971428,  0.59375286], [0.61314315,  0.58739132,  0.59174138,  0.,  0.51259488,  0.31161687,  0.30605486,  0.20593329], [0.39620084,  0.29837909,  0.35614809,  0.51259488,  0.,  0.30258512,  0.3078427,  0.40874094], [0.58555251,  0.50728887,  0.54230458,  0.31161687,  0.30258512,  0.,  0.02644245,  0.13084012], [0.58298725,  0.50780809,  0.53971428,  0.30605486,  0.3078427,  0.02644245,  0.,  0.12430177], [0.63058382,  0.57089478,  0.59375286,  0.20593329,  0.40874094,  0.13084012,  0.12430177,  0.]])
        self.assertEqual(idl_constrained_angle.shape, self.python_constrained_angle.shape)
        self.assertEqualFloatArray(idl_constrained_angle, self.python_constrained_angle, precision)

    def test_angle_reset(self):
        idl_reset_angle = np.array([[0.,  0.13652141,  0.07055733,  0.61314315,  0.39620084,  0.58555251,  0.58298725,  0.63058382], [0.13652141,  0.,  0.1167507 ,  0.58739132,  0.29837909,  0.50728887,  0.50780809,  0.57089478], [0.07055733,  0.1167507,  0.,  0.59174138,  0.35614809,  0.54230458,  0.53971428,  0.59375286], [0.61314315,  0.58739132,  0.59174138,  0.,  0.51259488,  0.31161687,  0.30605486,  0.20593329], [0.39620084,  0.29837909,  0.35614809,  0.51259488,  0.,  0.30258512,  0.3078427,  0.40874094], [0.58555251,  0.50728887,  0.54230458,  0.31161687,  0.30258512,  0.,  0.02644245,  0.13084012], [0.58298725,  0.50780809,  0.53971428,  0.30605486,  0.3078427,  0.02644245,  0.,  0.12430177], [0.63058382,  0.57089478,  0.59375286,  0.20593329,  0.40874094,  0.13084012,  0.12430177,  0.]])
        self.assertEqual(idl_reset_angle.shape, self.python_reset_angle.shape)
        self.assertEqualFloatArray(idl_reset_angle, self.python_reset_angle, precision)

    def test_constraints_constrained(self):
        idl_constrained_const = np.array([[0., 1., 1., 1., 2., 0., 0., 2.], [0., 0., 1., 0., 1., 0., 0., 1.], [0., 0., 0., 0., 1., 0., 0., 2.], [0., 1., 1., 0., 2., 0., 0., 1.], [0., 0., 0., 0., 0., 0., 0., 0.], [1., 1., 1., 1., 1., 0., 1., 1.], [1., 1., 1., 1., 1., 0., 0., 1.], [0., 0., 0., 0., 2., 0., 0., 0.]])
        self.assertEqual(idl_constrained_const.shape, self.python_constrained_const.shape)

        con1 = np.zeros(idl_constrained_const.shape, dtype=int)
        con2 = np.zeros(self.python_constrained_const.shape, dtype=int)
        con1[np.where(idl_constrained_const > 0)] = 100
        con2[np.where(self.python_constrained_const > 0)] = 100
        self.assertEqualFloatArray(con1, con2, precision)

    def test_constraints_reset(self):
        idl_reset_const = np.array([[0.,  1.,  1.,  1.,  2.,  0.,  0.,  2.], [0.,  0.,  1.,  0.,  2.,  0.,  0.,  1.], [0.,  0.,  0.,  0.,  2.,  0.,  0.,  2.], [ 0.,  1.,  2.,  0.,  2.,  0.,  0.,  1.], [0.,  0.,  0.,  0.,  0.,  0.,  0.,  0.], [1.,  2.,  2.,  1.,  2.,  0.,  1.,  2.], [1.,  1.,  2.,  1.,  2.,  0.,  0.,  2.], [0.,  0.,  0.,  0.,  2.,  0.,  0.,  0.]])
        self.assertEqual(idl_reset_const.shape, self.python_reset_const.shape)

        con1 = np.zeros(idl_reset_const.shape, dtype=int)
        con2 = np.zeros(self.python_reset_const.shape, dtype=int)
        con1[np.where(idl_reset_const > 0)] = 100
        con2[np.where(self.python_reset_const > 0)] = 100
        self.assertEqualFloatArray(con1, con2, precision)

    def test_error(self):
        self.assertEqual(self.error, "Constraints must be a tuple with 3 values. Set -9999 when not used.")
