# -*- coding: utf-8 -*-
"""
| ----------------------------------------------------------------------------------------------------------------------
| Date                : June 2020
| Copyright           : © 2020 by Ann Crabbé (KU Leuven)
| Email               : acrabbe.foss@gmail.com
|
| This file is part of the Spectral Libraries QGIS plugin and python package.
|
| This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
| License as published by the Free Software Foundation, either version 3 of the License, or any later version.
|
| This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
| warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
|
| You should have received a copy of the GNU General Public License (COPYING.txt). If not see www.gnu.org/licenses.
| ----------------------------------------------------------------------------------------------------------------------
"""
import pathlib

from spectral_libraries.interfaces.imports import import_image, import_library_as_array
from spectral_libraries.core.amuses import Amuses


""" ############################### MATLAB DATA ############################### """
# import xlrd
# from scipy.io import loadmat
#
# # load classes from EXCEL
# excel_file = 'data_music/Classification_scheme.xlsx'
# excel_sheet = xlrd.open_workbook(excel_file).sheet_by_name("Matlab")
# classes_level_4 = [excel_sheet.cell_value(r, 0) for r in range(excel_sheet.nrows)]
# classes_level_2 = [excel_sheet.cell_value(r, 2) for r in range(excel_sheet.nrows)]
#
# # load library from MATLAB
# matlab_library_file = loadmat('data_music/library_start.mat')
# bad_bands = list(range(0, 4)) + list(range(141, 163)) + list(range(192, 225)) + list(range(272, 285))
#
# matlab_library = matlab_library_file["lib_start_sm"]
# matlab_library = np.delete(matlab_library, bad_bands, axis=0)
#
# matlab_library_classes_L4 = matlab_library_file["lib_start_mat"][0]
# matlab_library_classes_L2 = [classes_level_2[classes_level_4.index(x)] for x in matlab_library_classes_L4]
#
# # load the image from MATLAB
# matlab_image_file = loadmat('data_music/image.mat')
# matlab_image = matlab_image_file["sim_image"]
# matlab_image = np.delete(matlab_image, bad_bands, axis=2)
# matlab_image = matlab_image.transpose(2,0,1)

""" ############################### PYTHON DATA ############################### """

library_path = (pathlib.Path(__file__).resolve().parent / 'data_music' / 'library.sli').as_posix()
image_path = (pathlib.Path(__file__).resolve().parent / 'data_music' / 'image').as_posix()

library, _, _ = import_library_as_array(library_path)
envi_image = import_image(image_path)

""" ############################### ALGORITHM ############################### """

result = Amuses().execute(image=envi_image, library=library)

stop = 1
