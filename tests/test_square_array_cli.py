# -*- coding: utf-8 -*-
"""
| ----------------------------------------------------------------------------------------------------------------------
| Date                : March 2020
| Copyright           : © 2020 by Ann Crabbé (KU Leuven)
| Email               : acrabbe.foss@gmail.com
|
| This file is part of the Spectral Libraries QGIS plugin and python package.
|
| This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
| License as published by the Free Software Foundation, either version 3 of the License, or any later version.
|
| This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
| warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
|
| You should have received a copy of the GNU General Public License (COPYING.txt). If not see www.gnu.org/licenses.
| ----------------------------------------------------------------------------------------------------------------------
"""
import os
import pathlib
import numpy as np
from unittest import TestCase

from spectral_libraries.interfaces.square_array_cli import create_parser, run_square_array
from spectral_libraries.interfaces.imports import import_square

precision = 4


class TestSquareArrayCLI(TestCase):

    library_path = (pathlib.Path(__file__).resolve().parent / 'data' / 'spectral_library.sli').as_posix()
    square_path = (pathlib.Path(__file__).resolve().parent / 'data' / 'spectral_library_sq.sqr').as_posix()

    @classmethod
    def setUpClass(cls):
        pass

    @classmethod
    def tearDownClass(cls):
        out1 = cls.square_path
        out2 = '{}.hdr'.format(os.path.splitext(out1)[0])
        out3 = '{}.aux.xml'.format(out1)
        if os.path.exists(out1):
            os.remove(out1)
        if os.path.exists(out2):
            os.remove(out2)
        if os.path.exists(out3):
            os.remove(out3)

    def assertEqualFloatArray(self, array1, array2, decimals):
        if array1.ndim == 1:
            pix = array1.shape[0]
        elif array1.ndim == 2:
            pix = array1.shape[0] * array1.shape[1]
        elif array1.ndim == 3:
            pix = array1.shape[0] * array1.shape[1] * array1.shape[2]
        else:
            assert 0

        array1_line = np.reshape(array1, pix)
        array2_line = np.reshape(array2, pix)
        for i in range(pix):
            self.assertAlmostEqual(array1_line[i], array2_line[i], places=decimals)

    def test_empty_args(self):
        """ User passes no args, should fail with SystemExit """
        with self.assertRaises(SystemExit):
            parser = create_parser()
            parser.parse_args([])

    def test_obligated_args(self):
        parser = create_parser()
        run_square_array(parser.parse_args([self.library_path]))
        # library: library_path, reflectance_scale: None
        # min_fraction: -0.05, max_fraction: 1.05, max_rmse: 0.025, reset_off: F, unconstrained: F
        # output=None, exclude_constraints: F, exclude_rmse: F, include_angle: F, include_fractions: F, include_shade: F

        square_array, spectra_names = import_square(self.square_path)
        self.assertTrue('spectral angle' not in square_array)
        self.assertTrue('em fraction' not in square_array)
        self.assertTrue('shade fraction' not in square_array)

        rmse = square_array['rmse']
        cons = square_array['constraints']

        check_rmse = np.array(
            [[0., 0.14878859, 0.21285033, 0.1426155, 0.4733454, 0.06662178, 0.07372007, 0.3052985],
             [0.02728874, 0., 0.06815041, 0.13734919, 0.3237492, 0.05856317, 0.06511546, 0.24267778],
             [0.01413532, 0.04130463, 0., 0.1382453, 0.2896378, 0.06221596, 0.0688151, 0.2511872],
             [0.1153826, 0.19958517, 0.2523026, 0., 0.45215088, 0.03695973, 0.04034694, 0.20139565],
             [0.07738075, 0.10423706, 0.1472576, 0.12154929, 0., 0.03592192, 0.04057511, 0.17844464],
             [0.11799514, 0.25157806, 0.32065666, 0.1331396, 0.54103965, 0., 0.00810305, 0.32389757],
             [0.11358921, 0.24159563, 0.3102578, 0.12139753, 0.52827305, 0.00318798, 0., 0.30993938],
             [0.11822502, 0.19161087, 0.23629206, 0.05067773, 0.29508325, 0.0157274, 0.01660237, 0.]])
        check_cons = np.array(
            [[0., 4., 4., 3., 4., 3., 3., 4.], [3., 0., 4., 3., 4., 3., 3., 4.], [0., 3., 0., 3., 4., 3., 3., 3.],
             [3., 4., 4., 0., 4., 3., 3., 4.], [3., 3., 3., 3., 0., 3., 3., 3.], [4., 4., 4., 4., 4., 0., 1., 4.],
             [4., 4., 4., 4., 4., 0., 0., 4.], [3., 3., 3., 3., 4., 0., 0., 0.]])

        self.assertEqual(check_rmse.shape, rmse.shape)
        self.assertEqualFloatArray(check_rmse, rmse, precision)

        self.assertEqual(check_cons.shape, cons.shape)
        self.assertEqualFloatArray(check_cons, cons, 1)

    def test_unconstrained(self):
        parser = create_parser()
        run_square_array(parser.parse_args([self.library_path, '--reflectance-scale=10000', '-u', '--include-fractions',
                                            '--include-shade', '--include-angle']))
        # library: library_path, reflectance_scale: 10000
        # min_fraction: -0.05, max_fraction: 1.05, max_rmse: 0.025, reset_off: F, unconstrained: T
        # output=None, exclude_constraints: F, exclude_rmse: F, include_angle: T, include_fractions: T, include_shade: T

        square_array, spectra_names = import_square(self.square_path)
        rmse = square_array['rmse']
        angle = square_array['spectral angle']
        frac = square_array['em fraction']
        shade = square_array['shade fraction']

        idl_frac = np.array(
            [[0., 1.75193202, 2.10109878, 1.01087344, 3.03911138, 0.50104344, 0.5575282, 1.80849814],
             [0.56022608, 0., 1.18299985, 0.58180302, 1.78057849, 0.29715636, 0.33000019, 1.0653975],
             [0.47357616, 0.83383828, 0., 0.48703575, 1.46585262, 0.24447148, 0.27199373, 0.88109517],
             [0.66167408, 1.19090497, 1.41437531, 0., 2.32270026, 0.46297279, 0.51520598, 1.77327406],
             [0.2800386, 0.51308125, 0.59926385, 0.32697684, 0., 0.17420529, 0.19319537, 0.62370116],
             [1.38624072, 2.57099843, 3.00087452, 1.95691228, 5.23062086, 0., 1.11045885, 3.69258285],
             [1.25003207, 2.31378126, 2.70563602, 1.76476872, 4.70088577, 0.89989877, 0., 3.32690287],
             [0.36071521, 0.66452563, 0.779697, 0.54034978, 1.3500551, 0.26620331, 0.29595959, 0.]])
        idl_shade = np.array(
            [[0., -0.75193202, -1.10109878, -0.01087344, -2.03911138, 0.49895656, 0.4424718, -0.80849814],
             [0.43977392, 0., -0.18299985, 0.41819698, -0.78057849, 0.70284367, 0.66999984, -0.0653975],
             [0.52642381, 0.16616172, 0., 0.51296425, -0.46585262, 0.75552851, 0.72800624, 0.11890483],
             [0.33832592, -0.19090497, -0.41437531, 0., -1.32270026, 0.53702724, 0.48479402, -0.77327406],
             [0.7199614, 0.48691875, 0.40073615, 0.67302316, 0., 0.8257947, 0.80680466, 0.37629884],
             [-0.38624072, -1.57099843, -2.00087452, -0.95691228, -4.23062086, 0., -0.11045885, -2.69258285],
             [-0.25003207, -1.31378126, -1.70563602, -0.76476872, -3.70088577, 0.10010123, 0., -2.32690287],
             [0.63928479, 0.33547437, 0.220303, 0.45965022, -0.3500551, 0.73379672, 0.70404041, 0.]])
        idl_rmse = np.array(
            [[0., 0.0048257, 0.00297738, 0.01426155, 0.02549161, 0.00666218, 0.00737201, 0.02647196],
             [0.00272887, 0., 0.00491983, 0.01373492, 0.01941824, 0.00585632, 0.00651155, 0.02426164],
             [0.00141353, 0.00413046, 0., 0.01382453, 0.02303106, 0.0062216, 0.00688151, 0.02511872],
             [0.01153826, 0.01965063, 0.02355875, 0., 0.03239593, 0.00369597, 0.00403469, 0.00918052],
             [0.00773808, 0.01042371, 0.01472576, 0.01215493, 0., 0.00359219, 0.00405751, 0.01784446],
             [0.01108149, 0.01722594, 0.02179776, 0.00759866, 0.01968365, 0., 0.00035414, 0.00585754],
             [0.01103857, 0.01724202, 0.02170397, 0.00746731, 0.02001479, 0.0003188, 0., 0.0055664],
             [0.0118225, 0.01916108, 0.0236292, 0.00506777, 0.02625374, 0.00157274, 0.00166024, 0.]])
        idl_angle = np.array(
            [[0., 0.13652141, 0.07055733, 0.61314315, 0.39620084, 0.58555251, 0.58298725, 0.63058382],
             [0.13652141, 0., 0.1167507, 0.58739132, 0.29837909, 0.50728887, 0.50780809, 0.57089478],
             [0.07055733, 0.1167507, 0., 0.59174138, 0.35614809, 0.54230458, 0.53971428, 0.59375286],
             [0.61314315, 0.58739132, 0.59174138, 0., 0.51259488, 0.31161687, 0.30605486, 0.20593329],
             [0.39620084, 0.29837909, 0.35614809, 0.51259488, 0., 0.30258512, 0.3078427, 0.40874094],
             [0.58555251, 0.50728887, 0.54230458, 0.31161687, 0.30258512, 0., 0.02644245, 0.13084012],
             [0.58298725, 0.50780809, 0.53971428, 0.30605486, 0.3078427, 0.02644245, 0., 0.12430177],
             [0.63058382, 0.57089478, 0.59375286, 0.20593329, 0.40874094, 0.13084012, 0.12430177, 0.]])

        self.assertEqual(idl_frac.shape, frac.shape)
        self.assertEqualFloatArray(idl_frac, frac, precision)

        self.assertEqual(idl_shade.shape, shade.shape)
        self.assertEqualFloatArray(idl_shade, shade, precision)

        self.assertEqual(idl_rmse.shape, rmse.shape)
        self.assertEqualFloatArray(idl_rmse, rmse, precision)

        self.assertEqual(idl_angle.shape, angle.shape)
        self.assertEqualFloatArray(idl_angle, angle, precision)

    def test_constrained(self):
        parser = create_parser()
        run_square_array(
            parser.parse_args([self.library_path, '--reflectance-scale=10000', '--min-fraction=0', '--max-fraction=1',
                               '--include-fractions', '--include-shade', '--include-angle', '-r']))
        # library: library_path, reflectance_scale: 10000
        # min_fraction: -0.05, max_fraction: 1.05, max_rmse: 0.025, reset_off: T, unconstrained: F
        # output=None, exclude_constraints: F, exclude_rmse: F, include_angle: T, include_fractions: T, include_shade: T

        square_array, spectra_names = import_square(self.square_path)
        rmse = square_array['rmse']
        angle = square_array['spectral angle']
        frac = square_array['em fraction']
        shade = square_array['shade fraction']
        const = square_array['constraints']

        idl_frac = np.array(
            [[0., 1.75193202, 2.10109878, 1.01087344, 3.03911138, 0.50104344, 0.5575282, 1.80849814],
             [0.56022608, 0., 1.18299985, 0.58180302, 1.78057849, 0.29715636, 0.33000019, 1.0653975],
             [0.47357616, 0.83383828, 0., 0.48703575, 1.46585262, 0.24447148, 0.27199373, 0.88109517],
             [0.66167408, 1.19090497, 1.41437531, 0., 2.32270026, 0.46297279, 0.51520598, 1.77327406],
             [0.2800386, 0.51308125, 0.59926385, 0.32697684, 0., 0.17420529, 0.19319537, 0.62370116],
             [1.38624072, 2.57099843, 3.00087452, 1.95691228, 5.23062086, 0., 1.11045885, 3.69258285],
             [1.25003207, 2.31378126, 2.70563602, 1.76476872, 4.70088577, 0.89989877, 0., 3.32690287],
             [0.36071521, 0.66452563, 0.779697, 0.54034978, 1.3500551, 0.26620331, 0.29595959, 0.]])
        idl_shade = np.array(
            [[0., -0.75193202, -1.10109878, -0.01087344, -2.03911138, 0.49895656, 0.4424718, -0.80849814],
             [0.43977392, 0., -0.18299985, 0.41819698, -0.78057849, 0.70284367, 0.66999984, -0.0653975],
             [0.52642381, 0.16616172, 0., 0.51296425, -0.46585262, 0.75552851, 0.72800624, 0.11890483],
             [0.33832592, -0.19090497, -0.41437531, 0., -1.32270026, 0.53702724, 0.48479402, -0.77327406],
             [0.7199614, 0.48691875, 0.40073615, 0.67302316, 0., 0.8257947, 0.80680466, 0.37629884],
             [-0.38624072, -1.57099843, -2.00087452, -0.95691228, -4.23062086, 0., -0.11045885, -2.69258285],
             [-0.25003207, -1.31378126, -1.70563602, -0.76476872, -3.70088577, 0.10010123, 0., -2.32690287],
             [0.63928479, 0.33547437, 0.220303, 0.45965022, -0.3500551, 0.73379672, 0.70404041, 0.]])
        idl_rmse = np.array(
            [[0., 0.0048257, 0.00297738, 0.01426155, 0.02549161, 0.00666218, 0.00737201, 0.02647196],
             [0.00272887, 0., 0.00491983, 0.01373492, 0.01941824, 0.00585632, 0.00651155, 0.02426164],
             [0.00141353, 0.00413046, 0., 0.01382453, 0.02303106, 0.0062216, 0.00688151, 0.02511872],
             [0.01153826, 0.01965063, 0.02355875, 0., 0.03239593, 0.00369597, 0.00403469, 0.00918052],
             [0.00773808, 0.01042371, 0.01472576, 0.01215493, 0., 0.00359219, 0.00405751, 0.01784446],
             [0.01108149, 0.01722594, 0.02179776, 0.00759866, 0.01968365, 0., 0.00035414, 0.00585754],
             [0.01103857, 0.01724202, 0.02170397, 0.00746731, 0.02001479, 0.0003188, 0., 0.0055664],
             [0.0118225, 0.01916108, 0.0236292, 0.00506777, 0.02625374, 0.00157274, 0.00166024, 0.]])
        idl_angle = np.array(
            [[0., 0.13652141, 0.07055733, 0.61314315, 0.39620084, 0.58555251, 0.58298725, 0.63058382],
             [0.13652141, 0., 0.1167507, 0.58739132, 0.29837909, 0.50728887, 0.50780809, 0.57089478],
             [0.07055733, 0.1167507, 0., 0.59174138, 0.35614809, 0.54230458, 0.53971428, 0.59375286],
             [0.61314315, 0.58739132, 0.59174138, 0., 0.51259488, 0.31161687, 0.30605486, 0.20593329],
             [0.39620084, 0.29837909, 0.35614809, 0.51259488, 0., 0.30258512, 0.3078427, 0.40874094],
             [0.58555251, 0.50728887, 0.54230458, 0.31161687, 0.30258512, 0., 0.02644245, 0.13084012],
             [0.58298725, 0.50780809, 0.53971428, 0.30605486, 0.3078427, 0.02644245, 0., 0.12430177],
             [0.63058382, 0.57089478, 0.59375286, 0.20593329, 0.40874094, 0.13084012, 0.12430177, 0.]])
        idl_const = np.array(
            [[0., 1., 1., 1., 2., 0., 0., 2.], [0., 0., 1., 0., 1., 0., 0., 1.], [0., 0., 0., 0., 1., 0., 0., 2.],
             [0., 1., 1., 0., 2., 0., 0., 1.], [0., 0., 0., 0., 0., 0., 0., 0.], [1., 1., 1., 1., 1., 0., 1., 1.],
             [1., 1., 1., 1., 1., 0., 0., 1.], [0., 0., 0., 0., 2., 0., 0., 0.]])

        self.assertEqual(idl_frac.shape, frac.shape)
        self.assertEqualFloatArray(idl_frac, frac, precision)

        self.assertEqual(idl_shade.shape, shade.shape)
        self.assertEqualFloatArray(idl_shade, shade, precision)

        self.assertEqual(idl_rmse.shape, rmse.shape)
        self.assertEqualFloatArray(idl_rmse, rmse, precision)

        self.assertEqual(idl_angle.shape, angle.shape)
        self.assertEqualFloatArray(idl_angle, angle, precision)

        self.assertEqual(idl_const.shape, const.shape)
        con1 = np.zeros(idl_const.shape, dtype=int)
        con2 = np.zeros(const.shape, dtype=int)
        con1[np.where(idl_const > 0)] = 100
        con2[np.where(const > 0)] = 100
        self.assertEqualFloatArray(con1, con2, precision)

    def test_reset(self):
        parser = create_parser()
        run_square_array(
            parser.parse_args([self.library_path, '--reflectance-scale=10000', '--min-fraction=0', '--max-fraction=1',
                               '--include-fractions', '--include-shade', '--include-angle']))
        # library: library_path, reflectance_scale: 10000
        # min_fraction: -0.05, max_fraction: 1.05, max_rmse: 0.025, reset_off: F, unconstrained: F
        # output=None, exclude_constraints: F, exclude_rmse: F, include_angle: T, include_fractions: T, include_shade: T

        square_array, spectra_names = import_square(self.square_path)
        rmse = square_array['rmse']
        angle = square_array['spectral angle']
        frac = square_array['em fraction']
        shade = square_array['shade fraction']
        const = square_array['constraints']

        idl_frac = np.array([[0., 1., 1., 1., 1., 0.50104344, 0.5575282, 1.],
                             [0.56022608, 0., 1., 0.58180302, 1., 0.29715636, 0.33000019, 1.],
                             [0.47357616, 0.83383828, 0., 0.48703575, 1., 0.24447148, 0.27199373, 0.88109517],
                             [0.66167408, 1., 1., 0., 1., 0.46297279, 0.51520598, 1.],
                             [0.2800386, 0.51308125, 0.59926385, 0.32697684, 0., 0.17420529, 0.19319537, 0.62370116],
                             [1., 1., 1., 1., 1., 0., 1., 1.], [1., 1., 1., 1., 1., 0.89989877, 0., 1.],
                             [0.36071521, 0.66452563, 0.779697, 0.54034978, 1., 0.26620331, 0.29595959, 0.]])
        idl_shade = np.array([[0., 0., 0., 0., 0., 0.49895656, 0.4424718, 0.],
                              [0.43977392, 0., 0., 0.41819698, 0., 0.70284367, 0.66999984, 0.],
                              [0.52642381, 0.16616172, 0., 0.51296425, 0., 0.75552851, 0.72800624, 0.11890483],
                              [0.33832592, 0., 0., 0., 0., 0.53702724, 0.48479402, 0.],
                              [0.7199614, 0.48691875, 0.40073615, 0.67302316, 0., 0.8257947, 0.80680466, 0.37629884],
                              [0., 0., 0., 0., 0., 0., 0., 0.], [0., 0., 0., 0., 0., 0.10010123, 0., 0.],
                              [0.63928479, 0.33547437, 0.220303, 0.45965022, 0., 0.73379672, 0.70404041, 0.]])
        idl_rmse = np.array([[0., 0.01583056, 0.02227817, 0.01426322, 0.04818232, 0.00666218, 0.00737201, 0.03104146],
                             [0.00272887, 0., 0.00814309, 0.01373492, 0.03381026, 0.00585632, 0.00651155, 0.0243722],
                             [0.00141353, 0.00413046, 0., 0.01382453, 0.03029089, 0.0062216, 0.00688151, 0.02511872],
                             [0.01153826, 0.02021219, 0.02569985, 0., 0.04608811, 0.00369597, 0.00403469, 0.02125002],
                             [0.00773808, 0.01042371, 0.01472576, 0.01215493, 0., 0.00359219, 0.00405751, 0.01784446],
                             [0.0120199, 0.02560042, 0.03251031, 0.01381318, 0.05466585, 0., 0.00137785, 0.03298273],
                             [0.01153518, 0.0246332, 0.03150771, 0.01267435, 0.05344754, 0.0003188, 0., 0.03165283],
                             [0.0118225, 0.01916108, 0.0236292, 0.00506777, 0.03059844, 0.00157274, 0.00166024, 0.]])
        idl_angle = np.array([[0., 0.13652141, 0.07055733, 0.61314315, 0.39620084, 0.58555251, 0.58298725, 0.63058382],
                              [0.13652141, 0., 0.1167507, 0.58739132, 0.29837909, 0.50728887, 0.50780809, 0.57089478],
                              [0.07055733, 0.1167507, 0., 0.59174138, 0.35614809, 0.54230458, 0.53971428, 0.59375286],
                              [0.61314315, 0.58739132, 0.59174138, 0., 0.51259488, 0.31161687, 0.30605486, 0.20593329],
                              [0.39620084, 0.29837909, 0.35614809, 0.51259488, 0., 0.30258512, 0.3078427, 0.40874094],
                              [0.58555251, 0.50728887, 0.54230458, 0.31161687, 0.30258512, 0., 0.02644245, 0.13084012],
                              [0.58298725, 0.50780809, 0.53971428, 0.30605486, 0.3078427, 0.02644245, 0., 0.12430177],
                              [0.63058382, 0.57089478, 0.59375286, 0.20593329, 0.40874094, 0.13084012, 0.12430177, 0.]])
        idl_const = np.array(
            [[0., 1., 1., 1., 2., 0., 0., 2.], [0., 0., 1., 0., 2., 0., 0., 1.], [0., 0., 0., 0., 2., 0., 0., 2.],
             [0., 1., 2., 0., 2., 0., 0., 1.], [0., 0., 0., 0., 0., 0., 0., 0.], [1., 2., 2., 1., 2., 0., 1., 2.],
             [1., 1., 2., 1., 2., 0., 0., 2.], [0., 0., 0., 0., 2., 0., 0., 0.]])

        self.assertEqual(idl_frac.shape, frac.shape)
        self.assertEqualFloatArray(idl_frac, frac, precision)

        self.assertEqual(idl_shade.shape, shade.shape)
        self.assertEqualFloatArray(idl_shade, shade, precision)

        self.assertEqual(idl_rmse.shape, rmse.shape)
        self.assertEqualFloatArray(idl_rmse, rmse, precision)

        self.assertEqual(idl_angle.shape, angle.shape)
        self.assertEqualFloatArray(idl_angle, angle, precision)

        self.assertEqual(idl_const.shape, const.shape)
        con1 = np.zeros(idl_const.shape, dtype=int)
        con2 = np.zeros(const.shape, dtype=int)
        con1[np.where(idl_const > 0)] = 100
        con2[np.where(const > 0)] = 100
        self.assertEqualFloatArray(con1, con2, precision)
