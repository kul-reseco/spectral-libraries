# -*- coding: utf-8 -*-
"""
| ----------------------------------------------------------------------------------------------------------------------
| Date                : September 2020
| Copyright           : © 2020 by Ann Crabbé (KU Leuven)
| Email               : acrabbe.foss@gmail.com
|
| This file is part of the Spectral Libraries QGIS plugin and python package.
|
| This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
| License as published by the Free Software Foundation, either version 3 of the License, or any later version.
|
| This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
| warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
|
| You should have received a copy of the GNU General Public License (COPYING.txt). If not see www.gnu.org/licenses.
| ----------------------------------------------------------------------------------------------------------------------
"""
import os
import pathlib
import numpy as np
from unittest import TestCase

from qgis.PyQt.QtCore import QTimer
from qgis.core import QgsApplication

from spectral_libraries.interfaces.imports import import_library, import_library_metadata, import_library_as_array, \
    import_square
from spectral_libraries.interfaces.cres_gui import CresWidget
from spectral_libraries.interfaces.ear_masa_cob_gui import EarMasaCobWidget
from spectral_libraries.interfaces.ies_gui import IesWidget
from spectral_libraries.interfaces.square_array_gui import SquareArrayWidget
from spectral_libraries.interfaces.music_gui import MusicWidget
# todo: amuses widget


def import_result(lib_path, summary_path=None):
    _, spectra_names, _ = import_library_as_array(lib_path, spectra_names=True)

    if summary_path:
        f = open(summary_path, 'r')
        summary = f.readlines()
    else:
        summary = None

    return spectra_names, summary


class TestGUI(TestCase):
    library_robe_path = (pathlib.Path(__file__).resolve().parent / 'data' / 'roberts_et_al_2017_final.sli').as_posix()
    metadata = 'LEVEL_2'
    library_path = (pathlib.Path(__file__).resolve().parent / 'data' / 'spectral_library.sli').as_posix()

    raster_path = (pathlib.Path(__file__).resolve().parent / 'data' / 'testdata').as_posix()

    emc_output_path = (pathlib.Path(__file__).resolve().parent / 'data' / 'emc_cli.sli').as_posix()
    ies_output_path_plain = (pathlib.Path(__file__).resolve().parent / 'data' / 'cli_ies_plain.sli').as_posix()
    ies_output_path_forced0 = (pathlib.Path(__file__).resolve().parent / 'data' / 'cli_ies_forced0.sli').as_posix()
    ies_output_path_forced55 = (pathlib.Path(__file__).resolve().parent / 'data' / 'cli_ies_forced55.sli').as_posix()
    ies_summary_path = (pathlib.Path(__file__).resolve().parent / 'data' / 'cli_ies_plain_summary.txt').as_posix()
    sqa_output_path = (pathlib.Path(__file__).resolve().parent / 'data' / 'spectral_library_sq.sqr').as_posix()

    fix = np.array([])

    @classmethod
    def setUpClass(cls):
        cls.app = QgsApplication([], True)
        cls.app.initQgis()

        # EMC
        widget = EarMasaCobWidget()
        widget._library_browse(cls.library_robe_path)
        widget.classDropDown.setCurrentIndex(8)
        widget.browseOut.lineEdit().setText(cls.emc_output_path)
        widget._run_ear_masa_cob()
        widget.close()
        spectral_library = import_library(cls.emc_output_path)
        cls.ear = np.array(import_library_metadata(spectral_library, metadata='EAR'), dtype=float)
        cls.masa = np.array(import_library_metadata(spectral_library, metadata='MASA'), dtype=float)
        cls.cob_in = np.array(import_library_metadata(spectral_library, metadata='InCOB'), dtype=float)
        cls.cob_out = np.array(import_library_metadata(spectral_library, metadata='OutCOB'), dtype=float)
        cls.cob_ratio = np.array(import_library_metadata(spectral_library, metadata='COBI'), dtype=float)

        # IES plain
        widget = IesWidget()
        widget._library_browse(cls.library_robe_path)
        widget.classDropDown.setCurrentIndex(8)
        widget.browseOut.lineEdit().setText(cls.ies_output_path_plain)
        widget._run_ies(test_mode=True)
        widget.close()
        cls.plain_subset, cls.plain_summary = import_result(cls.ies_output_path_plain, cls.ies_summary_path)

        # IES forced step 0
        widget = IesWidget()
        widget._library_browse(cls.library_robe_path)
        widget.classDropDown.setCurrentIndex(8)
        widget.browseOut.lineEdit().setText(cls.ies_output_path_forced0)
        widget.forcedGroup.setChecked(1)
        widget.forcedDropDown.toggleItemCheckState(4)
        widget.forcedDropDown.toggleItemCheckState(8)
        widget.forcedDropDown.toggleItemCheckState(24)
        widget.forcedDropDown.toggleItemCheckState(43)
        widget._run_ies(test_mode=True)
        widget.close()
        cls.forced0_subset, _ = import_result(cls.ies_output_path_forced0)

        # IES forced step 55
        widget = IesWidget()
        widget._library_browse(cls.library_robe_path)
        widget.classDropDown.setCurrentIndex(8)
        widget.browseOut.lineEdit().setText(cls.ies_output_path_forced55)
        widget.forcedGroup.setChecked(1)
        widget.forcedDropDown.toggleItemCheckState(4)
        widget.forcedDropDown.toggleItemCheckState(8)
        widget.forcedDropDown.toggleItemCheckState(24)
        widget.forcedDropDown.toggleItemCheckState(43)
        widget.forcedStep.setValue(55)
        widget._run_ies(test_mode=True)
        widget.close()
        cls.forced55_subset, _ = import_result(cls.ies_output_path_forced55)

    @classmethod
    def tearDownClass(cls):
        all_outputs = [cls.emc_output_path, cls.ies_output_path_plain, cls.ies_output_path_forced0,
                       cls.ies_output_path_forced55, cls.ies_summary_path, cls.sqa_output_path]

        for output in all_outputs:
            outputs = [output,
                       '{}.hdr'.format(os.path.splitext(output)[0]),
                       '{}.csv'.format(os.path.splitext(output)[0]),
                       '{}.aux.xml'.format(output),
                       '{}_summary.txt'.format(os.path.splitext(output)[0])]
            for out in outputs:
                if os.path.exists(out):
                    os.remove(out)

    def assertEqualStringArray(self, array1, array2):
        x = array1.shape[0]
        for i in range(x):
            self.assertEqual(array1[i], array2[i])

    def assertEqualFloatArray(self, array1, array2, decimals=3):
        if array1.ndim == 1:
            pix = array1.shape[0]
        elif array1.ndim == 2:
            pix = array1.shape[0] * array1.shape[1]
        elif array1.ndim == 3:
            pix = array1.shape[0] * array1.shape[1] * array1.shape[2]
        else:
            assert 0

        array1_line = np.reshape(array1, pix)
        array2_line = np.reshape(array2, pix)
        for i in range(pix):
            self.assertAlmostEqual(array1_line[i], array2_line[i], places=decimals)

    def test_cres_app_opens(self):
        widget = CresWidget()
        widget.show()

        QTimer.singleShot(1000, self.app.closeAllWindows)
        self.app.exec_()

    def test_emc_app_opens(self):
        widget = EarMasaCobWidget()
        widget.show()

        QTimer.singleShot(1000, self.app.closeAllWindows)
        self.app.exec_()

    def test_ies_gui_app_opens(self):
        widget = IesWidget()
        widget.show()

        QTimer.singleShot(1000, self.app.closeAllWindows)
        self.app.exec_()

    def test_music_app_opens(self):
        widget = MusicWidget()
        widget.show()

        QTimer.singleShot(1000, self.app.closeAllWindows)
        self.app.exec_()

    def test_sqa_app_opens(self):
        widget = SquareArrayWidget()
        widget.show()

        QTimer.singleShot(1000, self.app.closeAllWindows)
        self.app.exec_()

    def test_library_app_opens(self):
        from spectral_libraries.interfaces.spectral_library_gui import SpectralLibraryWidget as qpsWidget

        widget = qpsWidget(map_canvas=None)
        widget.show()

        QTimer.singleShot(1000, self.app.closeAllWindows)
        self.app.exec_()

    def test_library_app_opens_with_library(self):
        from spectral_libraries.interfaces.spectral_library_gui import SpectralLibraryWidget as qpsWidget

        library = import_library(self.library_path)
        widget = qpsWidget(spectral_library=library, map_canvas=None)
        widget.show()

        QTimer.singleShot(1000, self.app.closeAllWindows)
        self.app.exec_()

    def test_emc_app_ear(self):
        reference = np.array(
            [0.12350687384605408, 0.13433655670710973, 0.17393667357308523, 0.09000309876033238, 0.1261984407901764,
             0.11816929777463277, 0.10551280776659648, 0.03527543197075526, 0.04749047259489695, 0.06105677783489227,
             0.03273525834083557, 0.06359916499682836, 0.09010549386342366, 0.10042884945869446, 0.06617036958535512,
             0.23216454684734344, 0.17263635993003845, 0.13874833285808563, 0.21319638192653656, 0.1954522281885147,
             0.16770151257514954, 0.09550690650939941, 0.07069480419158936, 0.054684464420591085, 0.05367399539266314,
             0.10131147929600307, 0.04807367495128086, 0.11428861320018768, 0.08958468266895839, 0.04936530760356358,
             0.08956749240557353, 0.08729757865269978, 0.1907566338777542, 0.06881014009316762, 0.04283674558003744,
             0.042160426576932274, 0.038132784267266594, 0.036739359299341835, 0.03212186445792516, 0.04224346081415812,
             0.10310433308283488, 0.09990738828976949, 0.09397729237874348, 0.23238906264305115, 0.1432373821735382,
             0.10093935330708821, 0.19875924289226532, 0.11126022785902023, 0.1373497098684311, 0.1057325005531311,
             0.11070752888917923, 0.12166629731655121, 0.11321258544921875, 0.0953545942902565, 0.127226784825325,
             0.09255246073007584, 0.0949951708316803, 0.09454260766506195, 0.10097832977771759, 0.03366248309612274,
             0.08881962299346924, 0.05569128692150116, 0.06772507230440776, 0.11534726619720459, 0.09260227282842,
             0.17019157111644745, 0.16099826991558075, 0.16532351076602936, 0.15397900342941284, 0.14463214576244354,
             0.13650628924369812, 0.15423257648944855, 0.15344983339309692, 0.03343328833580017, 0.022931169718503952,
             0.028508644551038742, 0.04057726263999939, 0.09387544790903728, 0.15011685235159739, 0.11210531847817558,
             0.06651970318385533, 0.03881317377090454, 0.037460144077028544, 0.030303401606423513, 0.032128176518848965,
             0.04605706532796224, 0.11514427832194737, 0.10024651459285192, 0.13267078569957189, 0.21633264422416687,
             0.2027030885219574])

        self.assertEqual(reference.shape, self.ear.shape)
        self.assertEqualFloatArray(reference, self.ear)

    def test_emc_app_masa(self):
        reference = np.array(
            [0.22405717770258585, 0.14254602364131383, 0.19928903239113943, 0.17143900053841726, 0.2576643427213033,
             0.2756739060084025, 0.2463299830754598, 0.09814959764480591, 0.14462979634602866, 0.18378674983978271,
             0.10075360536575317, 0.262949926512582, 0.10230779647827148, 0.13909576336542764, 0.14725699027379355,
             0.24238450825214386, 0.25775769352912903, 0.3217720687389374, 0.23847602307796478, 0.23938094079494476,
             0.3059144914150238, 0.16266000270843506, 0.1620849541255406, 0.16721863406045095, 0.18308302334376744,
             0.1692603485924857, 0.1635256835392543, 0.24586248397827148, 0.1938035488128662, 0.1782841341836112,
             0.1393572191397349, 0.1630296011765798, 0.2544102072715759, 0.1874242623647054, 0.17164097229639688,
             0.18491586049397787, 0.17925443251927695, 0.14520397782325745, 0.1466478407382965, 0.1185872753461202,
             0.15529348452885947, 0.17284621795018515, 0.15468353033065796, 0.2309933304786682, 0.2694198191165924,
             0.11556207140286763, 0.3166508972644806, 0.2395981401205063, 0.3835434913635254, 0.28382694721221924,
             0.3329278230667114, 0.36992067098617554, 0.3440772294998169, 0.2560674548149109, 0.38641831278800964,
             0.26787659525871277, 0.281512975692749, 0.278055340051651, 0.3014688491821289, 0.14462566375732422,
             0.20939634243647257, 0.24432343244552612, 0.14919862151145935, 0.24734028180440268, 0.1609647274017334,
             0.32595574855804443, 0.30669936537742615, 0.34489062428474426, 0.30803996324539185, 0.26845455169677734,
             0.2576868236064911, 0.3302965760231018, 0.3507064878940582, 0.09824861586093903, 0.07494421303272247,
             0.10053691267967224, 0.10664870057787214, 0.20089872678120932, 0.11117265905652728, 0.15897042410714285,
             0.1235224860055106, 0.08349527631487165, 0.09916731289454869, 0.08809036016464233, 0.09388649463653564,
             0.18516828616460165, 0.23391083308628627, 0.22964399201529367, 0.27365102086748394, 0.29539182782173157,
             0.283914178609848])

        self.assertEqual(reference.shape, self.masa.shape)
        self.assertEqualFloatArray(reference, self.masa)

    def test_emc_app_cob_in(self):
        reference = np.array(
            [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 3.0, 2.0, 0.0, 0.0, 2.0, 0.0, 0.0, 0.0, 0.0, 0.0, 3.0, 0.0, 0.0, 3.0, 0.0,
             0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 3.0, 1.0, 6.0, 0.0, 0.0, 0.0, 0.0, 0.0, 2.0, 0.0, 0.0, 9.0, 0.0, 0.0, 0.0,
             0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 1.0, 1.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 9.0, 0.0, 0.0, 0.0,
             0.0, 0.0, 0.0, 0.0, 0.0, 2.0, 4.0, 4.0, 2.0, 0.0, 0.0, 1.0, 1.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 3.0,
             3.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0])
        self.assertEqual(reference.shape, self.cob_in.shape)
        self.assertEqualFloatArray(reference, self.cob_in)

    def test_emc_app_cob_out(self):
        reference = np.array(
            [0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 6, 0, 0, 0, 0, 1, 1, 0, 3, 1, 0, 0, 0, 0, 0, 3, 4, 4, 0, 0, 0, 0, 0, 0, 0,
             0, 0, 0, 1, 0, 0, 0, 0, 4, 0, 0, 2, 0, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 5, 6, 0,
             1, 0, 6, 1, 0, 0, 0, 0, 2, 0, 0, 10, 5, 0, 0, 0, 0, 0, 0])
        self.assertEqual(reference.shape, self.cob_out.shape)
        self.assertEqualFloatArray(reference, self.cob_out)

    def test_emc_app_cob_ratio(self):
        reference = np.array(
            [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.09090909090909091, 0.0,
             0.0, 0.030303030303030304, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.022222222222222223, 0.022727272727272728,
             0.016666666666666666, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
             0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
             0.0, 0.0, 0.024242424242424242, 0.020202020202020204, 0.0, 0.0, 0.0, 0.05555555555555555,
             0.3333333333333333, 0.0, 0.0, 0.0, 0.0, 0.0625, 0.0, 0.0, 0.0375, 0.075, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0])
        self.assertEqual(reference.shape, self.cob_ratio.shape)
        self.assertEqualFloatArray(reference, self.cob_ratio)

    def test_ies_gui_plain(self):
        summary = self.plain_summary[75:]
        kappas = []
        for line in summary:
            if 'Kappa at this point:' in line:
                kappa = str.split(line, ':')
                kappa = round(float(kappa[1]), 6)
                kappas.append(kappa)
        kappas = np.array(kappas)

        idl_subset = np.array(
            ['basketball_asphalt_field_n1_X1153_Y310', 'airport_asphalt_n2_X1182_Y630', 'bapi_I02_X650_Y127',
             'ndbnyg.002-', 'ndbnye.004-', 'ndbnof.002-', 'ndbnye.011-', 'commercial_roof_n1_X2712_Y146',
             'public_storage_n1_X1446_Y478', 'fscgye.028-', 'parking_structure_p50_concrete_X669_Y202', 'modrmg.004-',
             'spcsye.003-', 'phelps_asphalt&grey_gravel_X1073_Y813', 'golfcourse23_X4051_Y447',
             'golf_course_fairways_1_X2188_Y517', 'green_sports_field_5_X690_Y181', 'senesced_grass_4_X155_Y169',
             'fhzgmg.008-', 'patterson_storage_roof_n1_X1527_Y413', 'REI_white_painted_commercial_roof_n5_X2926_Y762',
             'fish_and_gamedepartment_white_painted_commercial_roof_n2_X2271_Y400',
             'white_painted_commercial_roof_n5_X2692_Y107', 'REI_white_painted_commercial_roof_n5_X2928_Y761',
             'white_painted_commercial_roof_n4_X772_Y589', 'white_painted_commercial_roof_n5_X2699_Y99',
             'sears_commercial_roof_n6_X2233_Y478', 'white_painted_commercial_roof_n2_X851_Y602', 'vtmgyg.006-',
             'PEAM_F01_X1566_Y124', 'RDP_86_X2985_Y343', 'red_tile_n4_X3545_Y723', 'buchanan_red_tile_X786_Y238',
             'red_tile_n4_X3551_Y721', 'red_tile_n2_X2047_Y394', 'RDP_115_X2055_Y368', 'RDP_117_X2491_Y213',
             'RDP_117_X2488_Y217', 'lrxnxx.002-', 'lcxnxx.008-', 'soil_I06_X362_Y211', 'soil_I05_X409_Y385',
             'vbmgue.002-', 'tennis_court_n1_X1320_Y400', 'rotrye.006-', 'fsfnof.002-'], dtype='<U100')
        idl_kappa = np.array([0.0821816, 0.150391, 0.189774, 0.226230, 0.263639, 0.299814, 0.334176, 0.364149, 0.388508,
                              0.412984, 0.435640, 0.455772, 0.476277, 0.497166, 0.511306, 0.524771, 0.537157, 0.549569,
                              0.562005, 0.574704, 0.601460, 0.616369, 0.633065, 0.645812, 0.658191, 0.670624, 0.682697,
                              0.694809, 0.707010, 0.719301, 0.731682, 0.744155, 0.756431, 0.768828, 0.781349, 0.793993,
                              0.806765, 0.819665, 0.832695, 0.845858, 0.859155, 0.872589, 0.886161, 0.886339, 0.900469,
                              0.914447])

        self.assertEqual(self.plain_subset.shape, idl_subset.shape)
        self.assertEqualStringArray(self.plain_subset, idl_subset)

        self.assertEqual(kappas.shape, idl_kappa.shape)
        self.assertEqualFloatArray(kappas, idl_kappa, 5)

    def test_ies_gui_forced0b(self):

        idl_subset = np.array(
            ['asphalt_road_surface_1_X2263_Y531', 'airport_asphalt_n2_X1182_Y630', 'BAPI_I05_X237_Y573',
             'bapi_I02_X650_Y127', 'ndbnyg.002-', 'ndbnye.004-', 'ndbnof.002-', 'ndbnye.011-',
             'commercial_roof_n1_X2712_Y146', 'public_storage_n1_X1446_Y478', 'fsctmg.032-', 'fscgye.028-',
             'parking_structure_p10_concrete_X836_Y237', 'parking_structure_p50_concrete_X669_Y202', 'modrmg.004-',
             'spmrye.005-', 'spcsye.003-', 'phelps_asphalt&grey_gravel_X1073_Y813', 'golfcourse23_X4051_Y447',
             'golf_course_fairways_1_X2188_Y517', 'green_sports_field_5_X690_Y181', 'senesced_grass_4_X155_Y169',
             'foznof.001-', 'fhzgmg.008-', 'patterson_storage_roof_n1_X1527_Y413',
             'white_painted_commercial_roof_n1_X629_Y130', 'REI_white_painted_commercial_roof_n5_X2926_Y762',
             'fish_and_gamedepartment_white_painted_commercial_roof_n2_X2271_Y400',
             'white_painted_commercial_roof_n5_X2692_Y107', 'REI_white_painted_commercial_roof_n5_X2928_Y761',
             'white_painted_commercial_roof_n1_X634_Y130', 'white_painted_commercial_roof_n4_X772_Y589',
             'white_painted_commercial_roof_n5_X2699_Y99', 'sears_commercial_roof_n6_X2233_Y478',
             'white_painted_commercial_roof_n2_X851_Y602', 'vtmgyg.006-', 'PEAM_F01_X1566_Y124', 'RDP_86_X2985_Y343',
             'red_tile_n4_X3545_Y723', 'buchanan_red_tile_X786_Y238', 'red_tile_n4_X3551_Y721',
             'red_tile_n2_X2047_Y394', 'RDP_115_X2055_Y368', 'RDP_117_X2491_Y213', 'RDP_117_X2488_Y217', 'lrxnxx.002-',
             'lcxnxx.008-', 'soil_I06_X362_Y211', 'soil_I05_X409_Y385', 'vbmgue.002-', 'tennis_court_n1_X1320_Y400',
             'rotrye.006-', 'fsfnof.002-'], dtype='<U100')

        self.assertEqual(self.forced0_subset.shape, idl_subset.shape)
        self.assertEqualStringArray(self.forced0_subset, idl_subset)

    def test_ies_gui_forced55(self):

        idl_subset = np.array(
            ['basketball_asphalt_field_n1_X1153_Y310', 'airport_asphalt_n2_X1182_Y630', 'bapi_I02_X650_Y127',
             'ndbnyg.002-', 'ndbnye.004-', 'ndbnof.002-', 'ndbnye.011-', 'commercial_roof_n1_X2712_Y146',
             'public_storage_n1_X1446_Y478', 'fscgye.028-', 'parking_structure_p50_concrete_X669_Y202', 'modrmg.004-',
             'spcsye.003-', 'phelps_asphalt&grey_gravel_X1073_Y813', 'golfcourse23_X4051_Y447',
             'golf_course_fairways_1_X2188_Y517', 'green_sports_field_5_X690_Y181', 'senesced_grass_4_X155_Y169',
             'fhzgmg.008-', 'patterson_storage_roof_n1_X1527_Y413', 'REI_white_painted_commercial_roof_n5_X2926_Y762',
             'fish_and_gamedepartment_white_painted_commercial_roof_n2_X2271_Y400',
             'white_painted_commercial_roof_n5_X2692_Y107', 'REI_white_painted_commercial_roof_n5_X2928_Y761',
             'white_painted_commercial_roof_n4_X772_Y589', 'white_painted_commercial_roof_n5_X2699_Y99',
             'sears_commercial_roof_n6_X2233_Y478', 'white_painted_commercial_roof_n2_X851_Y602', 'vtmgyg.006-',
             'PEAM_F01_X1566_Y124', 'RDP_86_X2985_Y343', 'red_tile_n4_X3545_Y723', 'buchanan_red_tile_X786_Y238',
             'red_tile_n4_X3551_Y721', 'red_tile_n2_X2047_Y394', 'RDP_115_X2055_Y368', 'RDP_117_X2491_Y213',
             'RDP_117_X2488_Y217', 'lrxnxx.002-', 'lcxnxx.008-', 'soil_I06_X362_Y211', 'soil_I05_X409_Y385',
             'vbmgue.002-', 'tennis_court_n1_X1320_Y400', 'rotrye.006-', 'fsfnof.002-'], dtype='<U100')

        self.assertEqual(self.forced55_subset.shape, idl_subset.shape)
        self.assertEqualStringArray(self.forced55_subset, idl_subset)

    def test_sqa_obligated_args(self):
        widget = SquareArrayWidget()
        widget._library_browse(self.library_path)
        widget._run_square_array()
        widget.close()

        square_array, spectra_names = import_square(self.sqa_output_path)
        self.assertTrue('spectral angle' not in square_array)
        self.assertTrue('em fraction' not in square_array)
        self.assertTrue('shade fraction' not in square_array)

        rmse = square_array['rmse']
        cons = square_array['constraints']

        check_rmse = np.array(
            [[0., 0.14878859, 0.21285033, 0.1426155, 0.4733454, 0.06662178, 0.07372007, 0.3052985],
             [0.02728874, 0., 0.06815041, 0.13734919, 0.3237492, 0.05856317, 0.06511546, 0.24267778],
             [0.01413532, 0.04130463, 0., 0.1382453, 0.2896378, 0.06221596, 0.0688151, 0.2511872],
             [0.1153826, 0.19958517, 0.2523026, 0., 0.45215088, 0.03695973, 0.04034694, 0.20139565],
             [0.07738075, 0.10423706, 0.1472576, 0.12154929, 0., 0.03592192, 0.04057511, 0.17844464],
             [0.11799514, 0.25157806, 0.32065666, 0.1331396, 0.54103965, 0., 0.00810305, 0.32389757],
             [0.11358921, 0.24159563, 0.3102578, 0.12139753, 0.52827305, 0.00318798, 0., 0.30993938],
             [0.11822502, 0.19161087, 0.23629206, 0.05067773, 0.29508325, 0.0157274, 0.01660237, 0.]])
        check_cons = np.array(
            [[0., 4., 4., 3., 4., 3., 3., 4.], [3., 0., 4., 3., 4., 3., 3., 4.], [0., 3., 0., 3., 4., 3., 3., 3.],
             [3., 4., 4., 0., 4., 3., 3., 4.], [3., 3., 3., 3., 0., 3., 3., 3.], [4., 4., 4., 4., 4., 0., 1., 4.],
             [4., 4., 4., 4., 4., 0., 0., 4.], [3., 3., 3., 3., 4., 0., 0., 0.]])

        self.assertEqual(check_rmse.shape, rmse.shape)
        self.assertEqualFloatArray(check_rmse, rmse, 4)

        self.assertEqual(check_cons.shape, cons.shape)
        self.assertEqualFloatArray(check_cons, cons, 1)

    def test_sqa_unconstrained(self):
        widget = SquareArrayWidget()
        widget._library_browse(self.library_path)
        widget.libraryScaleValue.setValue(10000)
        widget.constraintsGroup.setChecked(0)
        widget.outAngleCheck.setCheckState(1)
        widget.outFracCheck.setCheckState(1)
        widget.outShadeCheck.setCheckState(1)
        widget._run_square_array()
        widget.close()

        square_array, spectra_names = import_square(self.sqa_output_path)
        rmse = square_array['rmse']
        angle = square_array['spectral angle']
        frac = square_array['em fraction']
        shade = square_array['shade fraction']

        idl_frac = np.array(
            [[0., 1.75193202, 2.10109878, 1.01087344, 3.03911138, 0.50104344, 0.5575282, 1.80849814],
             [0.56022608, 0., 1.18299985, 0.58180302, 1.78057849, 0.29715636, 0.33000019, 1.0653975],
             [0.47357616, 0.83383828, 0., 0.48703575, 1.46585262, 0.24447148, 0.27199373, 0.88109517],
             [0.66167408, 1.19090497, 1.41437531, 0., 2.32270026, 0.46297279, 0.51520598, 1.77327406],
             [0.2800386, 0.51308125, 0.59926385, 0.32697684, 0., 0.17420529, 0.19319537, 0.62370116],
             [1.38624072, 2.57099843, 3.00087452, 1.95691228, 5.23062086, 0., 1.11045885, 3.69258285],
             [1.25003207, 2.31378126, 2.70563602, 1.76476872, 4.70088577, 0.89989877, 0., 3.32690287],
             [0.36071521, 0.66452563, 0.779697, 0.54034978, 1.3500551, 0.26620331, 0.29595959, 0.]])
        idl_shade = np.array(
            [[0., -0.75193202, -1.10109878, -0.01087344, -2.03911138, 0.49895656, 0.4424718, -0.80849814],
             [0.43977392, 0., -0.18299985, 0.41819698, -0.78057849, 0.70284367, 0.66999984, -0.0653975],
             [0.52642381, 0.16616172, 0., 0.51296425, -0.46585262, 0.75552851, 0.72800624, 0.11890483],
             [0.33832592, -0.19090497, -0.41437531, 0., -1.32270026, 0.53702724, 0.48479402, -0.77327406],
             [0.7199614, 0.48691875, 0.40073615, 0.67302316, 0., 0.8257947, 0.80680466, 0.37629884],
             [-0.38624072, -1.57099843, -2.00087452, -0.95691228, -4.23062086, 0., -0.11045885, -2.69258285],
             [-0.25003207, -1.31378126, -1.70563602, -0.76476872, -3.70088577, 0.10010123, 0., -2.32690287],
             [0.63928479, 0.33547437, 0.220303, 0.45965022, -0.3500551, 0.73379672, 0.70404041, 0.]])
        idl_rmse = np.array(
            [[0., 0.0048257, 0.00297738, 0.01426155, 0.02549161, 0.00666218, 0.00737201, 0.02647196],
             [0.00272887, 0., 0.00491983, 0.01373492, 0.01941824, 0.00585632, 0.00651155, 0.02426164],
             [0.00141353, 0.00413046, 0., 0.01382453, 0.02303106, 0.0062216, 0.00688151, 0.02511872],
             [0.01153826, 0.01965063, 0.02355875, 0., 0.03239593, 0.00369597, 0.00403469, 0.00918052],
             [0.00773808, 0.01042371, 0.01472576, 0.01215493, 0., 0.00359219, 0.00405751, 0.01784446],
             [0.01108149, 0.01722594, 0.02179776, 0.00759866, 0.01968365, 0., 0.00035414, 0.00585754],
             [0.01103857, 0.01724202, 0.02170397, 0.00746731, 0.02001479, 0.0003188, 0., 0.0055664],
             [0.0118225, 0.01916108, 0.0236292, 0.00506777, 0.02625374, 0.00157274, 0.00166024, 0.]])
        idl_angle = np.array(
            [[0., 0.13652141, 0.07055733, 0.61314315, 0.39620084, 0.58555251, 0.58298725, 0.63058382],
             [0.13652141, 0., 0.1167507, 0.58739132, 0.29837909, 0.50728887, 0.50780809, 0.57089478],
             [0.07055733, 0.1167507, 0., 0.59174138, 0.35614809, 0.54230458, 0.53971428, 0.59375286],
             [0.61314315, 0.58739132, 0.59174138, 0., 0.51259488, 0.31161687, 0.30605486, 0.20593329],
             [0.39620084, 0.29837909, 0.35614809, 0.51259488, 0., 0.30258512, 0.3078427, 0.40874094],
             [0.58555251, 0.50728887, 0.54230458, 0.31161687, 0.30258512, 0., 0.02644245, 0.13084012],
             [0.58298725, 0.50780809, 0.53971428, 0.30605486, 0.3078427, 0.02644245, 0., 0.12430177],
             [0.63058382, 0.57089478, 0.59375286, 0.20593329, 0.40874094, 0.13084012, 0.12430177, 0.]])

        self.assertEqual(idl_frac.shape, frac.shape)
        self.assertEqualFloatArray(idl_frac, frac, 4)

        self.assertEqual(idl_shade.shape, shade.shape)
        self.assertEqualFloatArray(idl_shade, shade, 4)

        self.assertEqual(idl_rmse.shape, rmse.shape)
        self.assertEqualFloatArray(idl_rmse, rmse, 4)

        self.assertEqual(idl_angle.shape, angle.shape)
        self.assertEqualFloatArray(idl_angle, angle, 4)

    def test_sqa_constrained(self):
        widget = SquareArrayWidget()
        widget._library_browse(self.library_path)
        widget.libraryScaleValue.setValue(10000)
        widget.minFracValue.setValue(0)
        widget.maxFracValue.setValue(1)
        widget.resetCheck.setChecked(0)
        widget.outAngleCheck.setCheckState(1)
        widget.outFracCheck.setCheckState(1)
        widget.outShadeCheck.setCheckState(1)
        widget._run_square_array()
        widget.close()

        square_array, spectra_names = import_square(self.sqa_output_path)
        rmse = square_array['rmse']
        angle = square_array['spectral angle']
        frac = square_array['em fraction']
        shade = square_array['shade fraction']
        const = square_array['constraints']

        idl_frac = np.array(
            [[0., 1.75193202, 2.10109878, 1.01087344, 3.03911138, 0.50104344, 0.5575282, 1.80849814],
             [0.56022608, 0., 1.18299985, 0.58180302, 1.78057849, 0.29715636, 0.33000019, 1.0653975],
             [0.47357616, 0.83383828, 0., 0.48703575, 1.46585262, 0.24447148, 0.27199373, 0.88109517],
             [0.66167408, 1.19090497, 1.41437531, 0., 2.32270026, 0.46297279, 0.51520598, 1.77327406],
             [0.2800386, 0.51308125, 0.59926385, 0.32697684, 0., 0.17420529, 0.19319537, 0.62370116],
             [1.38624072, 2.57099843, 3.00087452, 1.95691228, 5.23062086, 0., 1.11045885, 3.69258285],
             [1.25003207, 2.31378126, 2.70563602, 1.76476872, 4.70088577, 0.89989877, 0., 3.32690287],
             [0.36071521, 0.66452563, 0.779697, 0.54034978, 1.3500551, 0.26620331, 0.29595959, 0.]])
        idl_shade = np.array(
            [[0., -0.75193202, -1.10109878, -0.01087344, -2.03911138, 0.49895656, 0.4424718, -0.80849814],
             [0.43977392, 0., -0.18299985, 0.41819698, -0.78057849, 0.70284367, 0.66999984, -0.0653975],
             [0.52642381, 0.16616172, 0., 0.51296425, -0.46585262, 0.75552851, 0.72800624, 0.11890483],
             [0.33832592, -0.19090497, -0.41437531, 0., -1.32270026, 0.53702724, 0.48479402, -0.77327406],
             [0.7199614, 0.48691875, 0.40073615, 0.67302316, 0., 0.8257947, 0.80680466, 0.37629884],
             [-0.38624072, -1.57099843, -2.00087452, -0.95691228, -4.23062086, 0., -0.11045885, -2.69258285],
             [-0.25003207, -1.31378126, -1.70563602, -0.76476872, -3.70088577, 0.10010123, 0., -2.32690287],
             [0.63928479, 0.33547437, 0.220303, 0.45965022, -0.3500551, 0.73379672, 0.70404041, 0.]])
        idl_rmse = np.array(
            [[0., 0.0048257, 0.00297738, 0.01426155, 0.02549161, 0.00666218, 0.00737201, 0.02647196],
             [0.00272887, 0., 0.00491983, 0.01373492, 0.01941824, 0.00585632, 0.00651155, 0.02426164],
             [0.00141353, 0.00413046, 0., 0.01382453, 0.02303106, 0.0062216, 0.00688151, 0.02511872],
             [0.01153826, 0.01965063, 0.02355875, 0., 0.03239593, 0.00369597, 0.00403469, 0.00918052],
             [0.00773808, 0.01042371, 0.01472576, 0.01215493, 0., 0.00359219, 0.00405751, 0.01784446],
             [0.01108149, 0.01722594, 0.02179776, 0.00759866, 0.01968365, 0., 0.00035414, 0.00585754],
             [0.01103857, 0.01724202, 0.02170397, 0.00746731, 0.02001479, 0.0003188, 0., 0.0055664],
             [0.0118225, 0.01916108, 0.0236292, 0.00506777, 0.02625374, 0.00157274, 0.00166024, 0.]])
        idl_angle = np.array(
            [[0., 0.13652141, 0.07055733, 0.61314315, 0.39620084, 0.58555251, 0.58298725, 0.63058382],
             [0.13652141, 0., 0.1167507, 0.58739132, 0.29837909, 0.50728887, 0.50780809, 0.57089478],
             [0.07055733, 0.1167507, 0., 0.59174138, 0.35614809, 0.54230458, 0.53971428, 0.59375286],
             [0.61314315, 0.58739132, 0.59174138, 0., 0.51259488, 0.31161687, 0.30605486, 0.20593329],
             [0.39620084, 0.29837909, 0.35614809, 0.51259488, 0., 0.30258512, 0.3078427, 0.40874094],
             [0.58555251, 0.50728887, 0.54230458, 0.31161687, 0.30258512, 0., 0.02644245, 0.13084012],
             [0.58298725, 0.50780809, 0.53971428, 0.30605486, 0.3078427, 0.02644245, 0., 0.12430177],
             [0.63058382, 0.57089478, 0.59375286, 0.20593329, 0.40874094, 0.13084012, 0.12430177, 0.]])
        idl_const = np.array(
            [[0., 1., 1., 1., 2., 0., 0., 2.], [0., 0., 1., 0., 1., 0., 0., 1.], [0., 0., 0., 0., 1., 0., 0., 2.],
             [0., 1., 1., 0., 2., 0., 0., 1.], [0., 0., 0., 0., 0., 0., 0., 0.], [1., 1., 1., 1., 1., 0., 1., 1.],
             [1., 1., 1., 1., 1., 0., 0., 1.], [0., 0., 0., 0., 2., 0., 0., 0.]])

        self.assertEqual(idl_frac.shape, frac.shape)
        self.assertEqualFloatArray(idl_frac, frac, 4)

        self.assertEqual(idl_shade.shape, shade.shape)
        self.assertEqualFloatArray(idl_shade, shade, 4)

        self.assertEqual(idl_rmse.shape, rmse.shape)
        self.assertEqualFloatArray(idl_rmse, rmse, 4)

        self.assertEqual(idl_angle.shape, angle.shape)
        self.assertEqualFloatArray(idl_angle, angle, 4)

        self.assertEqual(idl_const.shape, const.shape)
        con1 = np.zeros(idl_const.shape, dtype=int)
        con2 = np.zeros(const.shape, dtype=int)
        con1[np.where(idl_const > 0)] = 100
        con2[np.where(const > 0)] = 100
        self.assertEqualFloatArray(con1, con2, 4)

    def test_sqa_reset(self):
        widget = SquareArrayWidget()
        widget._library_browse(self.library_path)
        widget.libraryScaleValue.setValue(10000)
        widget.minFracValue.setValue(0)
        widget.maxFracValue.setValue(1)
        widget.resetCheck.setChecked(1)
        widget.outAngleCheck.setCheckState(1)
        widget.outFracCheck.setCheckState(1)
        widget.outShadeCheck.setCheckState(1)
        widget._run_square_array()
        widget.close()

        square_array, spectra_names = import_square(self.sqa_output_path)
        rmse = square_array['rmse']
        angle = square_array['spectral angle']
        frac = square_array['em fraction']
        shade = square_array['shade fraction']
        const = square_array['constraints']

        idl_frac = np.array([[0., 1., 1., 1., 1., 0.50104344, 0.5575282, 1.],
                             [0.56022608, 0., 1., 0.58180302, 1., 0.29715636, 0.33000019, 1.],
                             [0.47357616, 0.83383828, 0., 0.48703575, 1., 0.24447148, 0.27199373, 0.88109517],
                             [0.66167408, 1., 1., 0., 1., 0.46297279, 0.51520598, 1.],
                             [0.2800386, 0.51308125, 0.59926385, 0.32697684, 0., 0.17420529, 0.19319537, 0.62370116],
                             [1., 1., 1., 1., 1., 0., 1., 1.], [1., 1., 1., 1., 1., 0.89989877, 0., 1.],
                             [0.36071521, 0.66452563, 0.779697, 0.54034978, 1., 0.26620331, 0.29595959, 0.]])
        idl_shade = np.array([[0., 0., 0., 0., 0., 0.49895656, 0.4424718, 0.],
                              [0.43977392, 0., 0., 0.41819698, 0., 0.70284367, 0.66999984, 0.],
                              [0.52642381, 0.16616172, 0., 0.51296425, 0., 0.75552851, 0.72800624, 0.11890483],
                              [0.33832592, 0., 0., 0., 0., 0.53702724, 0.48479402, 0.],
                              [0.7199614, 0.48691875, 0.40073615, 0.67302316, 0., 0.8257947, 0.80680466, 0.37629884],
                              [0., 0., 0., 0., 0., 0., 0., 0.], [0., 0., 0., 0., 0., 0.10010123, 0., 0.],
                              [0.63928479, 0.33547437, 0.220303, 0.45965022, 0., 0.73379672, 0.70404041, 0.]])
        idl_rmse = np.array([[0., 0.01583056, 0.02227817, 0.01426322, 0.04818232, 0.00666218, 0.00737201, 0.03104146],
                             [0.00272887, 0., 0.00814309, 0.01373492, 0.03381026, 0.00585632, 0.00651155, 0.0243722],
                             [0.00141353, 0.00413046, 0., 0.01382453, 0.03029089, 0.0062216, 0.00688151, 0.02511872],
                             [0.01153826, 0.02021219, 0.02569985, 0., 0.04608811, 0.00369597, 0.00403469, 0.02125002],
                             [0.00773808, 0.01042371, 0.01472576, 0.01215493, 0., 0.00359219, 0.00405751, 0.01784446],
                             [0.0120199, 0.02560042, 0.03251031, 0.01381318, 0.05466585, 0., 0.00137785, 0.03298273],
                             [0.01153518, 0.0246332, 0.03150771, 0.01267435, 0.05344754, 0.0003188, 0., 0.03165283],
                             [0.0118225, 0.01916108, 0.0236292, 0.00506777, 0.03059844, 0.00157274, 0.00166024, 0.]])
        idl_angle = np.array([[0., 0.13652141, 0.07055733, 0.61314315, 0.39620084, 0.58555251, 0.58298725, 0.63058382],
                              [0.13652141, 0., 0.1167507, 0.58739132, 0.29837909, 0.50728887, 0.50780809, 0.57089478],
                              [0.07055733, 0.1167507, 0., 0.59174138, 0.35614809, 0.54230458, 0.53971428, 0.59375286],
                              [0.61314315, 0.58739132, 0.59174138, 0., 0.51259488, 0.31161687, 0.30605486, 0.20593329],
                              [0.39620084, 0.29837909, 0.35614809, 0.51259488, 0., 0.30258512, 0.3078427, 0.40874094],
                              [0.58555251, 0.50728887, 0.54230458, 0.31161687, 0.30258512, 0., 0.02644245, 0.13084012],
                              [0.58298725, 0.50780809, 0.53971428, 0.30605486, 0.3078427, 0.02644245, 0., 0.12430177],
                              [0.63058382, 0.57089478, 0.59375286, 0.20593329, 0.40874094, 0.13084012, 0.12430177, 0.]])
        idl_const = np.array(
            [[0., 1., 1., 1., 2., 0., 0., 2.], [0., 0., 1., 0., 2., 0., 0., 1.], [0., 0., 0., 0., 2., 0., 0., 2.],
             [0., 1., 2., 0., 2., 0., 0., 1.], [0., 0., 0., 0., 0., 0., 0., 0.], [1., 2., 2., 1., 2., 0., 1., 2.],
             [1., 1., 2., 1., 2., 0., 0., 2.], [0., 0., 0., 0., 2., 0., 0., 0.]])

        self.assertEqual(idl_frac.shape, frac.shape)
        self.assertEqualFloatArray(idl_frac, frac, 4)

        self.assertEqual(idl_shade.shape, shade.shape)
        self.assertEqualFloatArray(idl_shade, shade, 4)

        self.assertEqual(idl_rmse.shape, rmse.shape)
        self.assertEqualFloatArray(idl_rmse, rmse, 4)

        self.assertEqual(idl_angle.shape, angle.shape)
        self.assertEqualFloatArray(idl_angle, angle, 4)

        self.assertEqual(idl_const.shape, const.shape)
        con1 = np.zeros(idl_const.shape, dtype=int)
        con2 = np.zeros(const.shape, dtype=int)
        con1[np.where(idl_const > 0)] = 100
        con2[np.where(const > 0)] = 100
        self.assertEqualFloatArray(con1, con2, 4)
