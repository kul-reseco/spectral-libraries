# -*- coding: utf-8 -*-
"""
| ----------------------------------------------------------------------------------------------------------------------
| Date                : March 2020
| Copyright           : © 2020 by Ann Crabbé (KU Leuven)
| Email               : acrabbe.foss@gmail.com
|
| This file is part of the Spectral Libraries QGIS plugin and python package.
|
| This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
| License as published by the Free Software Foundation, either version 3 of the License, or any later version.
|
| This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
| warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
|
| You should have received a copy of the GNU General Public License (COPYING.txt). If not see www.gnu.org/licenses.
| ----------------------------------------------------------------------------------------------------------------------
"""
import os
import pathlib
import numpy as np
from unittest import TestCase

from spectral_libraries.interfaces.imports import import_library, import_library_metadata
from spectral_libraries.interfaces.ear_masa_cob_cli import create_parser, run_emc


class TestEarMasaCobCLI(TestCase):
    library_path = (pathlib.Path(__file__).resolve().parent / 'data' / 'roberts_et_al_2017_final.sli').as_posix()
    output_path = (pathlib.Path(__file__).resolve().parent / 'data' / 'emc_cli.sli').as_posix()
    metadata = 'LEVEL_2'

    @classmethod
    def setUpClass(cls):
        parser = create_parser()
        run_emc(parser.parse_args([cls.library_path, cls.metadata, '-o', cls.output_path]))

        spectral_library = import_library(cls.output_path)

        cls.ear = np.array(import_library_metadata(spectral_library, metadata='EAR'), dtype=float)
        cls.masa = np.array(import_library_metadata(spectral_library, metadata='MASA'), dtype=float)
        cls.cob_in = np.array(import_library_metadata(spectral_library, metadata='InCOB'), dtype=float)
        cls.cob_out = np.array(import_library_metadata(spectral_library, metadata='OutCOB'), dtype=float)
        cls.cob_ratio = np.array(import_library_metadata(spectral_library, metadata='COBI'), dtype=float)

    @classmethod
    def tearDownClass(cls):
        out1 = cls.output_path
        out2 = '{}.hdr'.format(os.path.splitext(out1)[0])
        out3 = '{}.csv'.format(os.path.splitext(out1)[0])
        out4 = '{}.aux.xml'.format(out1)
        if os.path.exists(out1):
            os.remove(out1)
        if os.path.exists(out2):
            os.remove(out2)
        if os.path.exists(out3):
            os.remove(out3)
        if os.path.exists(out4):
            os.remove(out4)

    def assertEqualFloatArray(self, array1, array2):
        x = array1.shape[0]
        for i in range(x):
            self.assertAlmostEqual(array1[i], array2[i], places=5)

    def test_ear(self):
        reference = np.array(
            [0.12350687384605408, 0.13433655670710973, 0.17393667357308523, 0.09000309876033238, 0.1261984407901764,
             0.11816929777463277, 0.10551280776659648, 0.03527543197075526, 0.04749047259489695, 0.06105677783489227,
             0.03273525834083557, 0.06359916499682836, 0.09010549386342366, 0.10042884945869446, 0.06617036958535512,
             0.23216454684734344, 0.17263635993003845, 0.13874833285808563, 0.21319638192653656, 0.1954522281885147,
             0.16770151257514954, 0.09550690650939941, 0.07069480419158936, 0.054684464420591085, 0.05367399539266314,
             0.10131147929600307, 0.04807367495128086, 0.11428861320018768, 0.08958468266895839, 0.04936530760356358,
             0.08956749240557353, 0.08729757865269978, 0.1907566338777542, 0.06881014009316762, 0.04283674558003744,
             0.042160426576932274, 0.038132784267266594, 0.036739359299341835, 0.03212186445792516, 0.04224346081415812,
             0.10310433308283488, 0.09990738828976949, 0.09397729237874348, 0.23238906264305115, 0.1432373821735382,
             0.10093935330708821, 0.19875924289226532, 0.11126022785902023, 0.1373497098684311, 0.1057325005531311,
             0.11070752888917923, 0.12166629731655121, 0.11321258544921875, 0.0953545942902565, 0.127226784825325,
             0.09255246073007584, 0.0949951708316803, 0.09454260766506195, 0.10097832977771759, 0.03366248309612274,
             0.08881962299346924, 0.05569128692150116, 0.06772507230440776, 0.11534726619720459, 0.09260227282842,
             0.17019157111644745, 0.16099826991558075, 0.16532351076602936, 0.15397900342941284, 0.14463214576244354,
             0.13650628924369812, 0.15423257648944855, 0.15344983339309692, 0.03343328833580017, 0.022931169718503952,
             0.028508644551038742, 0.04057726263999939, 0.09387544790903728, 0.15011685235159739, 0.11210531847817558,
             0.06651970318385533, 0.03881317377090454, 0.037460144077028544, 0.030303401606423513, 0.032128176518848965,
             0.04605706532796224, 0.11514427832194737, 0.10024651459285192, 0.13267078569957189, 0.21633264422416687,
             0.2027030885219574])

        self.assertEqual(reference.shape, self.ear.shape)
        self.assertEqualFloatArray(reference, self.ear)

    def test_masa(self):
        reference = np.array(
            [0.22405717770258585, 0.14254602364131383, 0.19928903239113943, 0.17143900053841726, 0.2576643427213033,
             0.2756739060084025, 0.2463299830754598, 0.09814959764480591, 0.14462979634602866, 0.18378674983978271,
             0.10075360536575317, 0.262949926512582, 0.10230779647827148, 0.13909576336542764, 0.14725699027379355,
             0.24238450825214386, 0.25775769352912903, 0.3217720687389374, 0.23847602307796478, 0.23938094079494476,
             0.3059144914150238, 0.16266000270843506, 0.1620849541255406, 0.16721863406045095, 0.18308302334376744,
             0.1692603485924857, 0.1635256835392543, 0.24586248397827148, 0.1938035488128662, 0.1782841341836112,
             0.1393572191397349, 0.1630296011765798, 0.2544102072715759, 0.1874242623647054, 0.17164097229639688,
             0.18491586049397787, 0.17925443251927695, 0.14520397782325745, 0.1466478407382965, 0.1185872753461202,
             0.15529348452885947, 0.17284621795018515, 0.15468353033065796, 0.2309933304786682, 0.2694198191165924,
             0.11556207140286763, 0.3166508972644806, 0.2395981401205063, 0.3835434913635254, 0.28382694721221924,
             0.3329278230667114, 0.36992067098617554, 0.3440772294998169, 0.2560674548149109, 0.38641831278800964,
             0.26787659525871277, 0.281512975692749, 0.278055340051651, 0.3014688491821289, 0.14462566375732422,
             0.20939634243647257, 0.24432343244552612, 0.14919862151145935, 0.24734028180440268, 0.1609647274017334,
             0.32595574855804443, 0.30669936537742615, 0.34489062428474426, 0.30803996324539185, 0.26845455169677734,
             0.2576868236064911, 0.3302965760231018, 0.3507064878940582, 0.09824861586093903, 0.07494421303272247,
             0.10053691267967224, 0.10664870057787214, 0.20089872678120932, 0.11117265905652728, 0.15897042410714285,
             0.1235224860055106, 0.08349527631487165, 0.09916731289454869, 0.08809036016464233, 0.09388649463653564,
             0.18516828616460165, 0.23391083308628627, 0.22964399201529367, 0.27365102086748394, 0.29539182782173157,
             0.283914178609848])

        self.assertEqual(reference.shape, self.masa.shape)
        self.assertEqualFloatArray(reference, self.masa)

    def test_cob_in(self):
        reference = np.array(
            [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 3.0, 2.0, 0.0, 0.0, 2.0, 0.0, 0.0, 0.0, 0.0, 0.0, 3.0, 0.0, 0.0, 3.0, 0.0,
             0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 3.0, 1.0, 6.0, 0.0, 0.0, 0.0, 0.0, 0.0, 2.0, 0.0, 0.0, 9.0, 0.0, 0.0, 0.0,
             0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 1.0, 1.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 9.0, 0.0, 0.0, 0.0,
             0.0, 0.0, 0.0, 0.0, 0.0, 2.0, 4.0, 4.0, 2.0, 0.0, 0.0, 1.0, 1.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 3.0,
             3.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0])
        self.assertEqual(reference.shape, self.cob_in.shape)
        self.assertEqualFloatArray(reference, self.cob_in)

    def test_cob_out(self):
        reference = np.array(
            [0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 6, 0, 0, 0, 0, 1, 1, 0, 3, 1, 0, 0, 0, 0, 0, 3, 4, 4, 0, 0, 0, 0, 0, 0, 0,
             0, 0, 0, 1, 0, 0, 0, 0, 4, 0, 0, 2, 0, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 5, 6, 0,
             1, 0, 6, 1, 0, 0, 0, 0, 2, 0, 0, 10, 5, 0, 0, 0, 0, 0, 0])
        self.assertEqual(reference.shape, self.cob_out.shape)
        self.assertEqualFloatArray(reference, self.cob_out)

    def test_cob_ratio(self):
        reference = np.array(
            [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.09090909090909091, 0.0,
             0.0, 0.030303030303030304, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.022222222222222223, 0.022727272727272728,
             0.016666666666666666, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
             0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
             0.0, 0.0, 0.024242424242424242, 0.020202020202020204, 0.0, 0.0, 0.0, 0.05555555555555555,
             0.3333333333333333, 0.0, 0.0, 0.0, 0.0, 0.0625, 0.0, 0.0, 0.0375, 0.075, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0])
        self.assertEqual(reference.shape, self.cob_ratio.shape)
        self.assertEqualFloatArray(reference, self.cob_ratio)
